rlcopt = -O1 -Wall -Wextra -std=c99 -pedantic -D_DEFAULT_SOURCE -DPLATFORM_DESKTOP
rllink = -I/usr/local/include -L/usr/local/lib -lraylib -lGL -lm -lpthread -ldl -lrt -lX11
srcd = src/
bind = bin/

# Note: -lglfw when using raylib compiled with external GLFW lib

main: $(bind)main.o $(bind)mm.o $(bind)audio.o $(bind)video.o $(bind)main_menu.o $(bind)options.o $(bind)running.o $(bind)settings.o $(bind)background.o $(bind)rmistify.o
	gcc -o main $(bind)main.o $(bind)mm.o $(bind)audio.o $(bind)video.o $(bind)main_menu.o $(bind)options.o $(bind)running.o $(bind)settings.o $(bind)background.o $(bind)rmistify.o $(rllink)

$(bind)main.o: $(srcd)main.c $(srcd)main.h $(srcd)constants.h $(srcd)video.h $(srcd)mm.h $(srcd)audio.h $(srcd)settings.h $(srcd)background.h $(srcd)states/main_menu.h
	gcc -c $(rlcopt) -o $(bind)main.o $(srcd)main.c

$(bind)mm.o: $(srcd)mm.c $(srcd)mm.h $(srcd)constants.h $(srcd)video.h $(srcd)audio.h
	gcc -c $(rlcopt) -o $(bind)mm.o $(srcd)mm.c

$(bind)audio.o: $(srcd)audio.c $(srcd)audio.h
	gcc -c $(rlcopt) -o $(bind)audio.o $(srcd)audio.c

$(bind)video.o: $(srcd)video.c $(srcd)video.h $(srcd)constants.h $(srcd)main.h $(srcd)mm.h
	gcc -c $(rlcopt) -o $(bind)video.o $(srcd)video.c

$(bind)settings.o: $(srcd)settings.c $(srcd)settings.h $(srcd)constants.h
	gcc -c $(rlcopt) -o $(bind)settings.o $(srcd)settings.c

$(bind)background.o: $(srcd)background.c $(srcd)background.h $(srcd)constants.h $(srcd)lib/rmistify.h $(srcd)backgrounds.h
	gcc -c $(rlcopt) -o $(bind)background.o $(srcd)background.c

$(bind)rmistify.o: $(srcd)lib/rmistify.c $(srcd)lib/rmistify.h
	gcc -c $(rlcopt) -o $(bind)rmistify.o $(srcd)lib/rmistify.c

$(bind)main_menu.o: $(srcd)states/main_menu.c $(srcd)states/main_menu.h $(srcd)states/options.h $(srcd)states/running.h $(srcd)constants.h $(srcd)main.h $(srcd)mm.h $(srcd)audio.h $(srcd)settings.h $(srcd)lib/easings.h
	gcc -c $(rlcopt) -o $(bind)main_menu.o $(srcd)states/main_menu.c

$(bind)options.o: $(srcd)states/options.c $(srcd)states/options.h $(srcd)states/main_menu.h $(srcd)constants.h $(srcd)main.h $(srcd)mm.h $(srcd)settings.h $(srcd)background.h $(srcd)video.h $(srcd)audio.h
	gcc -c $(rlcopt) -o $(bind)options.o $(srcd)states/options.c

$(bind)running.o: $(srcd)states/running.c $(srcd)states/running.h $(srcd)states/main_menu.h $(srcd)constants.h $(srcd)main.h $(srcd)mm.h $(srcd)audio.h $(srcd)settings.h $(srcd)background.h
	gcc -c $(rlcopt) -o $(bind)running.o $(srcd)states/running.c
