/* bginitgen.c */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "../../src/lib/rmistify.h"

#define ARGS_DEBUG
#define NUM_FIGURES_WHITESPACE 8  // Whitespace reserved for writing the num_figures value once it is known. More figures than 10 power this value will break the file
#define DFT_INITIAL_INDENTATION 1  // Make this configureable via command line?

/* Manually copied from mm.h, maybe they should be in their own header */
#define V_CELLS 23
#define H_CELLS 32
#define S_V_CELLS (V_CELLS + 1)
#define S_H_CELLS H_CELLS
#define CELLSIZE 8.0f

enum {
	LABEL_NAME,
	LABEL_NUM_FIGURES,
	LABEL_ARGS,
	LABEL_CANVASW,
	LABEL_CANVASH,
	LABEL_CANVASW_VALUE,
	LABEL_CANVASH_VALUE,
	NUM_LABELS,
};

static const char *labels[] = {
	[LABEL_NAME] = "name",
	[LABEL_NUM_FIGURES] = "num_figures",
	[LABEL_ARGS] = "args",
	[LABEL_CANVASW] = "canvas_w",
	[LABEL_CANVASH] = "canvas_h",
	[LABEL_CANVASW_VALUE] = "CELLSIZE * H_CELLS",  // Make these two last configurable via command line?
	[LABEL_CANVASH_VALUE] = "CELLSIZE * V_CELLS",
};

static int indentation = DFT_INITIAL_INDENTATION;

static const struct {
	char *name;
	enum {INT_TYPE, FLT_TYPE} type;
	ptrdiff_t member;
} arginfos[] = {
	{.name = "numverts", .type = INT_TYPE, .member = offsetof(struct rmt_args, numverts)},
	{.name = "numpolys", .type = INT_TYPE, .member = offsetof(struct rmt_args, numpolys)},
	{.name = "vxmovmin", .type = INT_TYPE, .member = offsetof(struct rmt_args, vxmovmin)},
	{.name = "vxmovmax", .type = INT_TYPE, .member = offsetof(struct rmt_args, vxmovmax)},
	{.name = "vymovmin", .type = INT_TYPE, .member = offsetof(struct rmt_args, vymovmin)},
	{.name = "vymovmax", .type = INT_TYPE, .member = offsetof(struct rmt_args, vymovmax)},
	{.name = "separatn", .type = FLT_TYPE, .member = offsetof(struct rmt_args, separatn)},
	{.name = "speeddft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, speeddft)},
	{.name = "thickmin", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickmin)},
	{.name = "thickmax", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickmax)},
	{.name = "thickdft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickdft)},
	{.name = "thdelmin", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdelmin)},
	{.name = "thdelmax", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdelmax)},
	{.name = "thdeldft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdeldft)},
	{.name = "thickaut", .type = INT_TYPE, .member = offsetof(struct rmt_args, thickaut)},
	{.name = "thicmode", .type = INT_TYPE, .member = offsetof(struct rmt_args, thicmode)},
	{.name = "alphmode", .type = INT_TYPE, .member = offsetof(struct rmt_args, alphmode)},
	{.name = "color__r", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__r)},
	{.name = "color__g", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__g)},
	{.name = "color__b", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__b)},
	{.name = "clorcycl", .type = INT_TYPE, .member = offsetof(struct rmt_args, clorcycl)},
};


static void indent(FILE *fp);
static void increase_indent(FILE *fp);
static void decrease_indent(FILE *fp);
static int get_args_from_file(char *filename, FILE *fp_out);
static int read_figure_info(FILE *fp, struct rmt_args *figure_args);
static int write_figure(FILE *fp_out, struct rmt_args *args, int figure_num);


int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr, "Usage: bginitgen <input_filename_1> ... <input_filename_n> <output_filename>\n");

		return 0;
	}

	char *filename_in;
	char *filename_out;
	FILE *fp_out;

	filename_in = argv[1];
	filename_out = argv[argc - 1];

	fp_out = fopen(filename_out, "wt");
	if (fp_out == NULL) {
		fprintf(stderr, "Could not create file %s\n", filename_out);

		return 1;
	}

	for (int i = 1; i < argc - 1; ++i) {
		filename_in = argv[i];
		get_args_from_file(filename_in, fp_out);
	}

	fclose(fp_out);

	return 0;
}

static void indent(FILE *fp)
{
	for (int i = 0; i < indentation; ++i) {
		putc('\t', fp);
	}
}

static void increase_indent(FILE *fp)
{
	++indentation;

	indent(fp);
}

static void decrease_indent(FILE *fp)
{
	if (indentation <= 0) {
		return;
	}

	--indentation;

	indent(fp);
}

static int get_args_from_file(char *filename_in, FILE *fp_out)
{
	FILE *fp_in;
	struct rmt_args args;
	int read_figures = 0;
	long qty_value_pos;

	fp_in = fopen(filename_in, "rt");
	if (fp_in == NULL) {
		fprintf(stderr, "Could not open file %s\n", filename_in);

		return 1;
	}

	// Write opening section
	indent(fp_out);
	fprintf(fp_out, "{\n");
	increase_indent(fp_out);
	fprintf(fp_out, ".%s = \"\",\n", labels[LABEL_NAME]);  // Only prepares the field, data has to be filled manually (args files do not have figure names)
	indent(fp_out);
	fprintf(fp_out, ".%s = ", labels[LABEL_NUM_FIGURES]);
	qty_value_pos = ftell(fp_out);
	fprintf(fp_out, "%*s,\n", NUM_FIGURES_WHITESPACE, "");

	while (feof(fp_in) == 0) {
		if (read_figure_info(fp_in, &args) != 0) {
			break;
		}

		if (rmt_validate_args(&args) != 0) {
			break;
		}

		if (write_figure(fp_out, &args, read_figures) != 0) {
			break;
		}

		++read_figures;
	}

	// Write closing section and fill num_figures
	decrease_indent(fp_out);
	fprintf(fp_out, "},\n");
	{
		long cur_pos = ftell(fp_out);

		fseek(fp_out, qty_value_pos, SEEK_SET);
		fprintf(fp_out, "%d", read_figures);
		fseek(fp_out, cur_pos, SEEK_SET);
	}

	fclose(fp_in);

	return 0;
}

#define MAX_LINE_LEN 1024
static int read_figure_info(FILE *fp, struct rmt_args *figure_args)
{
	const ptrdiff_t figure_args_addr = (ptrdiff_t) &(*figure_args);
	char read_line[MAX_LINE_LEN];
	char *endptr;
	long intval;
	double fltval;

	for (size_t i = 0; i < sizeof(arginfos) / sizeof(arginfos[0]); ++i) {
		size_t slen = strlen(arginfos[i].name);

		if (fgets(read_line, MAX_LINE_LEN, fp) == NULL) {
			fprintf(stderr, "Error reading line %zu\n", i);
			return 3;
		}

		if (strncmp(read_line, arginfos[i].name, slen) != 0) {
			fprintf(stderr, "Bad argname for arg %zu\n", i);
			return 4;
		}

		if (arginfos[i].type == INT_TYPE) {
			long *member_ptr = (long *)(figure_args_addr + arginfos[i].member);

			errno = 0;
			intval = strtol(&read_line[slen], &endptr, 0);

			if (endptr == &read_line[slen] || errno != 0) {
				fprintf(stderr, "Error reading argument %zu\n", i);
				return 5;
			}

			*member_ptr = intval;
		}
		else {  // FLT_TYPE
			double *member_ptr =
				(double *)(figure_args_addr + arginfos[i].member);

			errno = 0;
			fltval = strtod(&read_line[slen], &endptr);

			if (endptr == &read_line[slen] || errno != 0) {
				fprintf(stderr, "Error reading argument %zu\n", i);
				return 6;
			}

			*member_ptr = fltval;
		}
	}

	figure_args->canvas_w = H_CELLS * CELLSIZE;
	figure_args->canvas_h = V_CELLS * CELLSIZE;

#ifdef ARGS_DEBUG
	for (size_t i = 0; i < sizeof(arginfos) / sizeof(arginfos[0]); ++i) {

		if (arginfos[i].type == INT_TYPE) {
			long *member_ptr = (long *)(figure_args_addr + arginfos[i].member);
			fprintf(stderr, "%s value %ld\n", arginfos[i].name, *member_ptr);
		}
		else {
			double *member_ptr =
				(double *)(figure_args_addr + arginfos[i].member);
			fprintf(stderr, "%s value %f\n", arginfos[i].name, *member_ptr);
		}
	}
#endif

	return 0;
}

static int write_figure(FILE *fp_out, struct rmt_args *args, int figure_num)
{
	const ptrdiff_t args_addr = (ptrdiff_t) &(*args);

	indent(fp_out);
	fprintf(fp_out, ".%s[%d] = {\n", labels[LABEL_ARGS], figure_num);  // read_num + starting_number, if the option gets implemented for single files...
	increase_indent(fp_out);
	fprintf(fp_out, ".%s = %s,\n", labels[LABEL_CANVASW], labels[LABEL_CANVASW_VALUE]);
	indent(fp_out);
	fprintf(fp_out, ".%s = %s,\n", labels[LABEL_CANVASH], labels[LABEL_CANVASH_VALUE]);

	for (size_t i = 0; i < sizeof(arginfos) / sizeof(arginfos[0]); ++i) {
		indent(fp_out);

		if (arginfos[i].type == INT_TYPE) {
			long *member_ptr = (long *)(args_addr + arginfos[i].member);

			fprintf(fp_out, ".%s = %ld,\n", arginfos[i].name, *member_ptr);
		}
		else {  // arginfos[i].type == FLT_TYPE
			double *member_ptr = (double *)(args_addr + arginfos[i].member);

			fprintf(fp_out, ".%s = %.4g,\n", arginfos[i].name, *member_ptr);
		}
	}

	decrease_indent(fp_out);
	fprintf(fp_out, "},\n");

	return 0;
}