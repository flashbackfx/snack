/* soundtest_tool.c */

#include "../../src/constants.h"
#include "../../src/audio.h"
#include "../../src/mm.h"
#include "../../src/video.h"  // Needed for the video set-up
#include <raylib.h>


#define MIN_SONG_VOL 0.0f
#define MAX_SONG_VOL 10.0f
#define MIN_SFX_VOL 0.0f
#define MAX_SFX_VOL 1.0f
#define SONG_VOL_STEP_BIG 1.0f
#define SONG_VOL_STEP_SMALL 0.1f
#define SFX_VOL_STEP_BIG 0.1f
#define SFX_VOL_STEP_SMALL 0.01f

#define KEY_NEXT_OPTION KEY_DOWN
#define KEY_PREV_OPTION KEY_UP
#define KEY_INCR KEY_RIGHT
#define KEY_DECR KEY_LEFT
#define KEY_INCR_FINE KEY_KP_6
#define KEY_DECR_FINE KEY_KP_4
#define KEY_PLAY KEY_ENTER
#define KEY_STOP KEY_SPACE


const char * const window_title = "snack: sound test tool";


static enum songs selected_song;
static enum sounds selected_sound;
static float song_volumes[NUM_SONGS];
static float sfx_volume = 0.0f;
static enum {
	ST_OPTION_SONG = 0,
	ST_OPTION_SONG_VOL,
	ST_OPTION_SFX,
	ST_OPTION_SFX_VOL,
	NUM_ST_OPTIONS,
} soundtest_option;

Font font;


static void init(void);
static void close(void);
static void soundtest_tool(void);
static void soundtest_entry(void);
static void soundtest_update(void);
static void change_sfx_volume(float volume);
static void soundtest_render(void);


int main(void)
{
	if (ChangeDirectory("../../") == false) {
		return 1;
	}

	init();
	run_loop(soundtest_tool);
	close();

	return 0;
}

// NOTE: load_graphics and unload_graphics have external linkage so the settings
//  module can find them (They are needed by the toggle_vsync function that is
//  not even needed here...)
void load_graphics(void)
{
	Image ifont;

	ifont = LoadImage("ZXfont.png");
	font = LoadFontFromImage(ifont, MAGENTA, ' ');
	UnloadImage(ifont);
}

void unload_graphics(void)
{
	UnloadFont(font);
}

static void init(void)
{
	SetConfigFlags(FLAG_WINDOW_HIDDEN);
	InitWindow(50, 10, "setting up");
	video_info_init();
	CloseWindow();
	SetConfigFlags(0);
	InitWindow(S_H_CELLS * CELLSIZE * *video_scale,
	           S_V_CELLS * CELLSIZE * *video_scale, window_title);
	SetTargetFPS(DFT_FPS);
	load_graphics();
	start_framebuffer(S_H_CELLS * CELLSIZE, S_V_CELLS * CELLSIZE);
	audio_init();
}

static void close(void)
{
	audio_close();
	stop_framebuffer();
	unload_graphics();
	CloseWindow();
}

static void soundtest_tool(void)
{
	transition_substate(
		soundtest_entry,
		soundtest_update,
		soundtest_render,
		empty_exit_action
	);
}

static void soundtest_entry(void)
{
	sfx_volume = sfx_mix_volume;
	for (int i = 0; i < NUM_SONGS; ++i) {
		song_volumes[i] = song_mix_volumes[i];
	}

	change_sfx_volume(sfx_volume);
}

static void soundtest_update(void)
{
	static bool sfx_vol_change = false;

	if (IsKeyPressed(KEY_STOP)) {
		audio_play_song(SONG_NONE);
	}

	if (IsKeyPressed(KEY_NEXT_OPTION)) {
		++soundtest_option;
		if (soundtest_option >= NUM_ST_OPTIONS) {
			soundtest_option = 0;
		}
	}
	if (IsKeyPressed(KEY_PREV_OPTION)) {
		if (soundtest_option == 0) {
			soundtest_option = NUM_ST_OPTIONS - 1;
		}
		else {
			--soundtest_option;
		}
	}

	switch (soundtest_option) {

	case ST_OPTION_SONG:
		if (IsKeyPressed(KEY_INCR)) {
			++selected_song;
			if (selected_song >= NUM_SONGS) {
				selected_song = 0;
			}
		}
		else
		if (IsKeyPressed(KEY_DECR)) {
			if (selected_song == 0) {
				selected_song = NUM_SONGS - 1;
			}
			else {
				--selected_song;
			}
		}
		else
		if (IsKeyPressed(KEY_PLAY)) {
			audio_play_song(selected_song);  // No pause, no restart
			audio_set_song_volume(song_volumes[selected_song]);
		}

		break;

	case ST_OPTION_SONG_VOL:
		{
			float cur_song_vol = song_volumes[selected_song];

			if (IsKeyPressed(KEY_INCR) &&
				cur_song_vol <= MAX_SONG_VOL - SONG_VOL_STEP_BIG)
			{
				cur_song_vol += SONG_VOL_STEP_BIG;
			}
			else
			if (IsKeyPressed(KEY_DECR) &&
				cur_song_vol >= MIN_SONG_VOL + SONG_VOL_STEP_BIG)
			{
				cur_song_vol -= SONG_VOL_STEP_BIG;
			}
			else
			if (IsKeyPressed(KEY_INCR_FINE) &&
				cur_song_vol <= MAX_SONG_VOL - SONG_VOL_STEP_SMALL)
			{
				cur_song_vol += SONG_VOL_STEP_SMALL;
			}
			else
			if (IsKeyPressed(KEY_DECR_FINE) &&
				cur_song_vol >= MIN_SONG_VOL + SONG_VOL_STEP_SMALL)
			{
				cur_song_vol -= SONG_VOL_STEP_SMALL;
			}

			if (song_volumes[selected_song] != cur_song_vol) {
				song_volumes[selected_song] = cur_song_vol;
				audio_set_song_volume(cur_song_vol);
			}
		}

		break;

	case ST_OPTION_SFX:
		if (sfx_vol_change == true) {
			change_sfx_volume(sfx_volume);
			sfx_vol_change = false;
		}

		if (IsKeyPressed(KEY_INCR)) {
			++selected_sound;
			if (selected_sound >= NUM_SOUNDS) {
				selected_sound = 0;
			}
		}
		else
		if (IsKeyPressed(KEY_DECR)) {
			if (selected_sound == 0) {
				selected_sound = NUM_SOUNDS - 1;
			}
			else {
				--selected_sound;
			}
		}
		else
		if (IsKeyPressed(KEY_PLAY)) {
			PlaySound(sounds[selected_sound]);
		}

		break;

	case ST_OPTION_SFX_VOL:
		{
			float cur_sfx_volume = sfx_volume;

			if (IsKeyPressed(KEY_INCR) &&
				cur_sfx_volume <= MAX_SFX_VOL - SFX_VOL_STEP_BIG)
			{
				cur_sfx_volume += SFX_VOL_STEP_BIG;
			}
			else
			if (IsKeyPressed(KEY_DECR) &&
				cur_sfx_volume >= MIN_SFX_VOL + SFX_VOL_STEP_BIG)
			{
				cur_sfx_volume -= SFX_VOL_STEP_BIG;
			}
			else
			if (IsKeyPressed(KEY_INCR_FINE) &&
				cur_sfx_volume <= MAX_SFX_VOL - SFX_VOL_STEP_SMALL)
			{
				cur_sfx_volume += SFX_VOL_STEP_SMALL;
			}
			else
			if (IsKeyPressed(KEY_DECR_FINE) &&
				cur_sfx_volume >= MIN_SFX_VOL + SFX_VOL_STEP_SMALL)
			{
				cur_sfx_volume -= SFX_VOL_STEP_SMALL;
			}

			if (sfx_volume != cur_sfx_volume) {
				sfx_volume = cur_sfx_volume;
				sfx_vol_change = true;
			}
		}

		break;

	default:
		break;

	}

	audio_update_music();
}

static void change_sfx_volume(float volume)
{
	for (int i = 0; i < NUM_SOUNDS; ++i) {
		SetSoundVolume(sounds[i], volume);
	}
}

static void soundtest_render(void)
{
	int x = 0;
	int y = 0;
	int line = 0;

	DrawTextEx(font, TextFormat("SONG %d", selected_song),
	           (Vector2){CELLSIZE * x, CELLSIZE * (y + line++)}, 8, 0,
	           soundtest_option == ST_OPTION_SONG ? RED : WHITE);
	DrawTextEx(font, TextFormat("SONG VOLUME %f", song_volumes[selected_song]),
	           (Vector2){CELLSIZE * x, CELLSIZE * (y + line++)}, 8, 0,
	           soundtest_option == ST_OPTION_SONG_VOL ? RED : WHITE);
	DrawTextEx(font, TextFormat("SOUND %d", selected_sound),
	           (Vector2){CELLSIZE * x, CELLSIZE * (y + line++)}, 8, 0,
	           soundtest_option == ST_OPTION_SFX ? RED : WHITE);
	DrawTextEx(font, TextFormat("SFX VOLUME %f", sfx_volume),
	           (Vector2){CELLSIZE * x, CELLSIZE * (y + line++)}, 8, 0,
	           soundtest_option == ST_OPTION_SFX_VOL ? RED : WHITE);
}