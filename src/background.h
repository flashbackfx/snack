/* background.h */

#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <stdbool.h>

extern const int num_backgrounds;

void background_select(int bg_id);
int background_selected(void);
int background_loaded(void);
bool background_option_active(void);
void background_option_set_to(bool toggle);
bool background_selection_is_random(void);
const char *background_get_name(int bg_id);
int background_set(void);
int background_stop(void);
int background_change(int bg_id);
void background_update(void);
void background_update_colorcycle(void);
void background_render(void);

#endif