/* audio.c */

#include "audio.h"
#include <stddef.h>
#include <raylib.h>


//#define OGG_MUSIC

#define MIN_SFX_USER_VOLUME 0
#define MAX_SFX_USER_VOLUME 10
#define MIN_MUSIC_USER_VOLUME 0
#define MAX_MUSIC_USER_VOLUME 10


Sound sounds[NUM_SOUNDS];
static Music songs[NUM_SONGS];

const float sfx_mix_volume = 0.8f;
const float song_mix_volumes[NUM_SONGS] = {
	[SONG_TITLE_SCREEN] = 1.7f,
	[SONG_GAMEPLAY_1] = 3.4f,
	[SONG_GAMEPLAY_2] = 3.4f,
};
static int sfx_user_volume = 10;
static int music_user_volume = 10;


static const char *song_sources[NUM_SONGS] = {
#ifndef OGG_MUSIC
	[SONG_TITLE_SCREEN] = "music/00.xm",
	[SONG_GAMEPLAY_1] = "music/01.xm",
	[SONG_GAMEPLAY_2] = "music/02.xm",
#else
	[SONG_TITLE_SCREEN] = "ogg/00.ogg",
	[SONG_GAMEPLAY_1] = "ogg/01.ogg",
	[SONG_GAMEPLAY_2] = "ogg/02.ogg",
#endif
};

static const Music music_none = {0};

static enum songs current_song = SONG_NONE;  // Current song identifier
static Music current_music = music_none;     // Current song music stream
static bool audio_initialized = false;


void audio_init(void)
{
	if (audio_initialized == true) {
		return;
	}

	InitAudioDevice();

	sounds[SOUND_EAT_FRUIT] = LoadSound("sound/002.wav");
	sounds[SOUND_SPRINT_READY] = LoadSound("sound/001.wav");
	sounds[SOUND_SPRINT_ACTIVE] = LoadSound("sound/003.wav");
	sounds[SOUND_SNAKE_COLLISION] = LoadSound("sound/005.wav");
	sounds[SOUND_TIME_OUT] = LoadSound("sound/006.wav");
	sounds[SOUND_READY] = LoadSound("sound/010.wav");
	sounds[SOUND_GO] = LoadSound("sound/011.wav");
	sounds[SOUND_MENU_ENTER] = LoadSound("sound/022.wav");
	sounds[SOUND_MENU_BACK] = LoadSound("sound/023.wav");
	sounds[SOUND_MENU_VERT] = LoadSound("sound/020.wav");
	sounds[SOUND_MENU_HORI] = LoadSound("sound/021.wav");

	const float sfx_final_volume = sfx_mix_volume * (sfx_user_volume * 0.1f);

	for (int i = 0; i < NUM_SOUNDS; ++i) {
		SetSoundVolume(sounds[i], sfx_final_volume);
	}

	for (int i = 0; i < NUM_SONGS; ++i) {
		songs[i] = LoadMusicStream(song_sources[i]);
	#ifndef OGG_MUSIC
		SetMusicVolume(songs[i], song_mix_volumes[i] *
		                         (music_user_volume * 0.1f));
	#endif
	}

	audio_initialized = true;
}

void audio_play_song(enum songs song)
{
	// Does not restart songs
	if (song == current_song) {
		return;
	}

	StopMusicStream(songs[current_song]);

	if (song >= NUM_SONGS) {
		current_song = SONG_NONE;
		current_music = music_none;
		return;
	}

	current_song = song;
	current_music = songs[current_song];

	PlayMusicStream(current_music);

	return;
}

bool audio_is_music_playing(void)
{
	return IsMusicStreamPlaying(current_music);
}

/* Function used by the soundtest_tool */
void audio_set_song_volume(float volume)
{
	SetMusicVolume(current_music, volume);
}

int audio_get_sfx_user_volume(void)
{
	return sfx_user_volume;
}

int audio_get_music_user_volume(void)
{
	return music_user_volume;
}

int audio_set_sfx_user_volume(int user_volume)
{
	if (user_volume < MIN_SFX_USER_VOLUME) {
		user_volume = MIN_SFX_USER_VOLUME;
	}
	else
	if (user_volume > MAX_SFX_USER_VOLUME) {
		user_volume = MAX_SFX_USER_VOLUME;
	}

	sfx_user_volume = user_volume;

	float new_volume = sfx_mix_volume * (user_volume * 0.1f);

	for (int i = 0; i < NUM_SOUNDS; ++i) {
		SetSoundVolume(sounds[i], new_volume);
	}

	return user_volume;
}

int audio_set_music_user_volume(int user_volume)
{
	if (user_volume < MIN_MUSIC_USER_VOLUME) {
		user_volume = MIN_MUSIC_USER_VOLUME;
	}
	else
	if (user_volume > MAX_MUSIC_USER_VOLUME) {
		user_volume = MAX_MUSIC_USER_VOLUME;
	}

	music_user_volume = user_volume;

	for (int i = 0; i < NUM_SONGS; ++i) {
		SetMusicVolume(songs[i], song_mix_volumes[i] * (user_volume * 0.1f));
	}

	return user_volume;
}

void audio_update_music(void)
{
	UpdateMusicStream(current_music);
}

void audio_close(void)
{
	if (audio_initialized == false) {
		return;
	}

	audio_play_song(SONG_NONE);
	for (int i = 0; i < NUM_SONGS; ++i) {
		UnloadMusicStream(songs[i]);
	}
	for (int i = 0; i < NUM_SOUNDS; ++i) {
		UnloadSound(sounds[i]);
	}
	CloseAudioDevice();

	audio_initialized = false;
}
