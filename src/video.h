/* video.h */

#ifndef VIDEO_H
#define VIDEO_H

#include <raylib.h>


extern const int * const video_scale;
extern const Vector2 * const video_origin;
extern const bool * const video_vsync;

extern void (*video_change_callback)(void);


void video_info_init(void);
int video_set_scale(int scale);
void video_toggle_fullscreen(void);
void video_toggle_vsync(void);


#endif