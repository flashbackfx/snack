/* mm.c */

#include "mm.h"
#include "constants.h"
#include "video.h"
#include "audio.h"
#include <stdio.h>  // Debug
#include <raylib.h>


#define ACTION_STACK_SIZE 2


long long cur_frame = 0;  // Debug
static int fpstoggle = 0;  // Debug
static int gridtoggle = 0;  // Debug

static RenderTexture2D fb_tex;
static bool loop_running = false;
static bool exit_game_signal = false;

static void (*entry_action[ACTION_STACK_SIZE])(void);
static void (*update)(void);
static void (*render_func)(void);
static void (*exit_action[ACTION_STACK_SIZE])(void);
static void (*next_update)(void);
static void (*next_render_func)(void);
static void (*next_exit_action[ACTION_STACK_SIZE])(void);
static int entry_stack = 0;
static int exit_stack = 0;
static int next_exit_stack = 0;
static bool substate_transition = false;

static struct {
	float fb_scaled_width;
	float fb_scaled_height;
	Rectangle frame_rectangle;
} render_constants;


static void debug_control(void);
static void debug_grid_render(void);
static void update_render_constants(void);
static void render(void (*render_func)(void));


static void update_render_constants(void)
{
	fprintf(stderr, "--Updating render constants--\n");

	render_constants.fb_scaled_width = fb_tex.texture.width * *video_scale;
	render_constants.fb_scaled_height = fb_tex.texture.height * *video_scale;
	render_constants.frame_rectangle.x = -(video_origin->x + 1.0f);
	render_constants.frame_rectangle.y = -(video_origin->y + 1.0f);
	render_constants.frame_rectangle.width = fb_tex.texture.width * *video_scale + 2.0f;
	render_constants.frame_rectangle.height = fb_tex.texture.height * *video_scale + 2.0f;
}

int start_framebuffer(int width, int height)
{
	UnloadRenderTexture(fb_tex);  // Does nothing if there's no RenderTexture loaded
	fb_tex = LoadRenderTexture(width, height);
	SetTextureFilter(fb_tex.texture, TEXTURE_FILTER_POINT);

	return 0;
}

int stop_framebuffer(void)
{
	UnloadRenderTexture(fb_tex);

	return 0;
}

static void debug_control(void)
{
	if (IsKeyPressed(KEY_W)) {
		fpstoggle = 1 - fpstoggle;
		if (fpstoggle)
			SetTargetFPS(1);
		else
			SetTargetFPS(60);
	}
	if (IsKeyPressed(KEY_E)) {
		gridtoggle = 1 - gridtoggle;
	}
}

static void debug_grid_render(void)
{
	if (gridtoggle == 1) {
		for (int y = 0; y < S_V_CELLS; ++y) {
			for (int x = 0; x < S_H_CELLS; ++x) {
				DrawLine(x * CELLSIZE_I, 0, x * CELLSIZE_I, S_V_CELLS * CELLSIZE_I, RED);
			}
			DrawLine(0, y * CELLSIZE_I, S_H_CELLS * CELLSIZE_I, y * CELLSIZE_I, RED);
		}
	}
}

int run_loop(void (*initial_state)(void))
{
	if (loop_running == true) {
		return 1;
	}

	update_render_constants();
	video_change_callback = update_render_constants;

	loop_running = true;
	(*initial_state)();

	while (!exit_game_signal && !WindowShouldClose()) {
		debug_control();

		if (substate_transition == true) {
			while (exit_stack > 0) {
				(*exit_action[--exit_stack])();
			}

			update = next_update;
			render_func = next_render_func;

			while (next_exit_stack > 0) {
				exit_action[exit_stack++] = next_exit_action[--next_exit_stack];
			}

			while (entry_stack > 0) {
				(*entry_action[--entry_stack])();
			}

			substate_transition = false;
		}

		audio_update_music();
		(*update)();
		render(render_func);

		++cur_frame;
	}

	loop_running = false;

	return 0;
}

static void render(void (*render_func)(void))
{
	BeginDrawing();
	BeginTextureMode(fb_tex);
	ClearBackground(BLACK);

	(*render_func)();

	debug_grid_render();

	EndTextureMode();

	ClearBackground(BLACK);

	DrawTexturePro(fb_tex.texture,
                   (Rectangle){0.0f, 0.0f, fb_tex.texture.width,
	                                       -fb_tex.texture.height},
	               (Rectangle){0.0f, 0.0f, render_constants.fb_scaled_width,
	                                       render_constants.fb_scaled_height},
	               *video_origin, 0.0f, WHITE);
	DrawRectangleLinesEx(render_constants.frame_rectangle, 1, RED);  // Helps see the screen boundaries in fullscreen mode

	EndDrawing();
}

void transition_substate(void (*new_entry_action)(void),
                         void (*new_update)(void),
                         void (*new_render_func)(void),
                         void (*new_exit_action)(void))
{
	entry_action[entry_stack++] = new_entry_action;
	next_update = new_update;
	next_render_func = new_render_func;
	next_exit_action[next_exit_stack++] = new_exit_action;

	substate_transition = true;
}

void push_entry_action(void (*new_entry_action)(void))
{
	if (entry_stack >= ACTION_STACK_SIZE) {
		TraceLog(LOG_WARNING, "Entry actions stack is full");
		return;
	}

	entry_action[entry_stack++] = new_entry_action;
}

void push_exit_action(void (*new_exit_action)(void))
{
	if (next_exit_stack >= ACTION_STACK_SIZE) {
		TraceLog(LOG_WARNING, "Next exit actions stack is full");
		return;
	}

	next_exit_action[next_exit_stack++] = new_exit_action;
}

void exit_game(void)
{
	exit_game_signal = true;
}

void empty_exit_action(void)
{
}

void empty_entry_action(void)
{
}
