/* video.c */

#include "video.h"
#include "constants.h"
#include "main.h"  // load_graphics(), unload_graphics(), window_title
#include "mm.h"
#include <stddef.h>
#include <raylib.h>
#include <stdio.h>  // Debug

#define DFT_VIDEO_SCALE 2
#define DFT_VIDEO_SCALE_MAX 4


static struct {
	int scale;
	int scale_windowed;
	int scale_max;
	bool fullscreen;
	bool vsync;
	int monitor_width;
	int monitor_height;
	Vector2 origin;
} video = {
	.scale = DFT_VIDEO_SCALE,
	.scale_windowed = DFT_VIDEO_SCALE,
	.scale_max = DFT_VIDEO_SCALE_MAX,
	.fullscreen = false,
	.vsync = false,
	.origin = {.x = 0.0f, .y = 0.0f},
};


const int * const video_scale = &video.scale;
const Vector2 * const video_origin = &video.origin;
const bool * const video_vsync = &video.vsync;

void (*video_change_callback)(void);

static void video_info_update(void);
static void enter_fullscreen_mode(void);
static void exit_fullscreen_mode(void);

static void video_info_update(void)
{
	video.monitor_width = GetMonitorWidth(0);
	video.monitor_height = GetMonitorHeight(0);

	int maxscale_x = video.monitor_width / (S_H_CELLS * CELLSIZE);
	int maxscale_y = video.monitor_height / (S_V_CELLS * CELLSIZE);

	if (maxscale_x < maxscale_y) {
		video.scale_max = maxscale_x;
	}
	else {
		video.scale_max = maxscale_y;
	}

	if (video.scale_max < 1) {
		// There was an error gathering monitor data
		video.scale_max = DFT_VIDEO_SCALE_MAX;
	}

	printf("monitor_width=%d\n", video.monitor_width);
	printf("monitor_height=%d\n", video.monitor_height);
	printf("maxscale_x=%d\n", maxscale_x);
	printf("maxscale_y=%d\n", maxscale_y);
	printf("scale_max=%d\n", video.scale_max);
}

void video_info_init(void)
{
	video_info_update();

	video.scale = video.scale_max - 1;
	video.scale_windowed = video.scale_max - 1;

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}

int video_set_scale(int scale)
{
	video_info_update();

	if (scale < 1 || scale > video.scale_max || video.fullscreen == true) {
		return video.scale;
	}

	video.scale_windowed = scale;
	video.scale = scale;

	int total_x = S_H_CELLS * CELLSIZE_I * scale;
	int total_y = S_V_CELLS * CELLSIZE_I * scale;

	SetWindowSize(total_x, total_y);

	total_x = (video.monitor_width / 2) - (total_x / 2);
	total_y = (video.monitor_height / 2) - (total_y / 2);

	SetWindowPosition(total_x, total_y);

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}

	return scale;
}

void video_toggle_fullscreen(void)
{
	video_info_update();

	if (video.monitor_width < 1 || video.monitor_height < 1) {
		return;
	}

	video.fullscreen = !video.fullscreen;

	if (video.fullscreen) {
		enter_fullscreen_mode();
	}
	else {
		exit_fullscreen_mode();
	}

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}

// FSCREEN_MODE:
//   Undefined: Fullscreen mode disabled
//   Value 0: Borderless fullscreen mode
//   Value 1: Exclusive fullscreen mode
//   Value 2: Exclusive fullscreen mode with video resolution switching
#define FSCREEN_MODE 0

#ifndef FSCREEN_MODE

static void enter_fullscreen_mode(void)
{

}

static void exit_fullscreen_mode(void)
{

}

#elif FSCREEN_MODE == 0

static void enter_fullscreen_mode(void)
{
	video.scale = video.scale_max;
	video.origin.x = -(video.monitor_width - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
	video.origin.y = -(video.monitor_height - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;

	SetWindowPosition(0, 0);
	SetWindowSize(video.monitor_width, video.monitor_height);
	HideCursor();
	SetWindowState(FLAG_WINDOW_UNDECORATED);
	SetWindowState(FLAG_WINDOW_TOPMOST);
}

static void exit_fullscreen_mode(void)
{
	video.origin.x = 0.0f;
	video.origin.y = 0.0f;

	video_set_scale(video.scale_windowed);
	ShowCursor();
	ClearWindowState(FLAG_WINDOW_TOPMOST);
	ClearWindowState(FLAG_WINDOW_UNDECORATED);
}

#elif FSCREEN_MODE == 1

static void enter_fullscreen_mode(void)
{
	video.scale = video.scale_max;
	video.origin.x = -(video.monitor_width - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
	video.origin.y = -(video.monitor_height - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;

	SetWindowState(FLAG_FULLSCREEN_MODE);
	SetWindowSize(video.monitor_width, video.monitor_height);
	HideCursor();
}

static void exit_fullscreen_mode(void)
{
	ClearWindowState(FLAG_FULLSCREEN_MODE);
	video.origin.x = 0.0f;
	video.origin.y = 0.0f;
	ShowCursor();
	video_set_scale(video.scale_windowed);
}

#elif FSCREEN_MODE == 2

static void enter_fullscreen_mode(void)
{
	SetWindowState(FLAG_FULLSCREEN_MODE);
	// To properly center the screen functions returning the horizontal and
	// vertical resolution of the current video mode should be used instead of
	// GetScreenWidth() and GetScreenHeight(). Raylib does not seem to currently
	// seem to have such functions.
	video.origin.x = -(GetScreenWidth() - S_H_CELLS * CELLSIZE * video.scale) / 2.0f;
	video.origin.y = -(GetScreenHeight() - S_V_CELLS * CELLSIZE * video.scale) / 2.0f;
	HideCursor();
}

static void exit_fullscreen_mode(void)
{
	ClearWindowState(FLAG_FULLSCREEN_MODE);
	video.origin.x = 0.0f;
	video.origin.y = 0.0f;
	ShowCursor();
	video_set_scale(video.scale_windowed);
}

#endif

void video_toggle_vsync(void)
{
	video.vsync = !video.vsync;

	if (video.vsync) {
		SetWindowState(FLAG_VSYNC_HINT);
	}
	else {
		ClearWindowState(FLAG_VSYNC_HINT);
	}

#if (FSCREEN_MODE == 1 || FSCREEN_MODE == 2)
	if (video.fullscreen && !IsWindowState(FLAG_FULLSCREEN_MODE)) {
		enter_fullscreen_mode();
	}
#endif

	if (video_change_callback != NULL) {
		(*video_change_callback)();
	}
}
