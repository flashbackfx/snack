/* options.c */

#include "options.h"
#include "main_menu.h"
#include "../constants.h"
#include "../main.h"
#include "../mm.h"
#include "../settings.h"
#include "../background.h"
#include "../video.h"
#include "../audio.h"
#include <raylib.h>


#define MUSIC_VOL_VARIATION 1
#define SFX_VOL_VARIATION 1


enum options_states {
	STATE_OPTIONS,
	STATE_BACKGROUND_SELECT,
	STATE_VIDEO_OPTIONS,
	STATE_SOUND_OPTIONS,
};

enum game_options {
	OPTION_NONE = -1,
	OPTION_LEVEL,
	OPTION_WALLS,
	OPTION_BACKGROUND,
	OPTION_CHOOSE_BACKGROUND,
	OPTION_VIDEO,
	OPTION_SOUND,
	OPTION_RESET,
	NUM_OPTIONS,
};

enum video_options {
	VOPT_NONE = -1,
	VOPT_SCALE,
	VOPT_MODE,
	VOPT_VSYNC,
	NUM_VIDEO_OPTS,
};

enum sound_options {
	SOPT_NONE = -1,
	SOPT_MUSIC_VOLUME,
	SOPT_SFX_VOLUME,
	SOPT_SOUNDTEST_MUSIC,
	SOPT_SOUNDTEST_SFX,
	NUM_SOUND_OPTS,
};


static enum game_options option = 0;
static enum rank difficulty;
static bool walls;
static bool background;
static int selected_background;
static int pointed_background;  // Used in the STATE_BACKGROUND_SELECT substate
static enum video_options video_option = 0;
static enum sound_options sound_option = 0;
static int cur_music_volume;
static int cur_sfx_volume;
static enum songs selected_song = 0;
static enum sounds selected_sound = 0;


static void change_options_state(enum options_states new_ostate);
static void options_menu_entry(void);
static void options_update(void);
static void options_render(void);
static void options_text(const int x, const int y);
static void choose_background_entry(void);
static void choose_background_update(void);
static void choose_background_render(void);
static void video_options_update(void);
static void video_options_render(void);
static void video_options_text(const int x, const int y);
static void sound_options_entry(void);
static void sound_options_update(void);
static void sound_options_render(void);
static void sound_options_text(const int x, const int y);


void options_menu(void)
{
	change_options_state(STATE_OPTIONS);
	push_entry_action(options_menu_entry);
}

static void change_options_state(enum options_states new_ostate)
{
	switch (new_ostate) {
	case STATE_OPTIONS:
		transition_substate(
			empty_entry_action,
			options_update,
			options_render,
			empty_exit_action
		);

		break;

	case STATE_BACKGROUND_SELECT:
		transition_substate(
			choose_background_entry,
			choose_background_update,
			choose_background_render,
			empty_exit_action
		);

		break;

	case STATE_VIDEO_OPTIONS:
		transition_substate(
			empty_entry_action,
			video_options_update,
			video_options_render,
			empty_exit_action
		);

		break;

	case STATE_SOUND_OPTIONS:
		transition_substate(
			sound_options_entry,
			sound_options_update,
			sound_options_render,
			empty_exit_action
		);

		break;
	}
}

static void options_menu_entry(void)
{
	difficulty = stt_get_rank();
	walls = stt_walls_are_active();
	background = background_option_active();
	selected_background = background_selected();
}

static void options_update(void)
{
	if (IsKeyPressed(KEY_GOBACK)) {
		PlaySound(sounds[SOUND_MENU_BACK]);
		main_menu();
		return;
	}

	if (IsKeyPressed(KEY_UP))  --option;
	else
	if (IsKeyPressed(KEY_DOWN))  ++option;

	if (option < 0)  option = 0;
	else
	if (option >= NUM_OPTIONS)  option = NUM_OPTIONS - 1;
	else
	if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_DOWN)) {
		PlaySound(sounds[SOUND_MENU_VERT]);
	}

	switch (option) {
	case OPTION_LEVEL:
		if (IsKeyPressed(KEY_LEFT) && difficulty > 0) {
			--difficulty;
			stt_set_rank(difficulty);
			PlaySound(sounds[SOUND_MENU_HORI]);
		}
		else
		if (IsKeyPressed(KEY_RIGHT) && difficulty < NUM_RANKS - 1) {
			++difficulty;
			stt_set_rank(difficulty);
			PlaySound(sounds[SOUND_MENU_HORI]);
		}

		break;

	case OPTION_WALLS:
		if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT) ||
		    IsKeyPressed(KEY_OK))
		{
			walls = !walls;
			stt_set_walls_option_to(walls);
			PlaySound(sounds[SOUND_MENU_HORI]);
		}

		break;

	case OPTION_BACKGROUND:
		if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT) ||
		    IsKeyPressed(KEY_OK))
		{
			background = !background;
			background_option_set_to(background);
			PlaySound(sounds[SOUND_MENU_HORI]);
		}

		break;

	case OPTION_CHOOSE_BACKGROUND:
		if (background == false) {
			if (IsKeyPressed(KEY_UP))  --option;
			else
			if (IsKeyPressed(KEY_DOWN))  ++option;
		}
		if (IsKeyPressed(KEY_OK)) {
			change_options_state(STATE_BACKGROUND_SELECT);
			PlaySound(sounds[SOUND_MENU_ENTER]);
		}

		break;

	case OPTION_VIDEO:
		if (IsKeyPressed(KEY_OK)) {
			change_options_state(STATE_VIDEO_OPTIONS);
			PlaySound(sounds[SOUND_MENU_ENTER]);
		}

		break;

	case OPTION_SOUND:
		if (IsKeyPressed(KEY_OK)) {
			change_options_state(STATE_SOUND_OPTIONS);
			PlaySound(sounds[SOUND_MENU_ENTER]);
		}

		break;

	case OPTION_RESET:
		if (IsKeyPressed(KEY_OK)) {
			stt_reset_score();
			PlaySound(sounds[SOUND_MENU_HORI]);
		}

		break;

	default:
		break;
	}
}

static void options_render(void)
{
	options_text(0, 0);
}

static void options_text(const int x, const int y)
{
	static const char * const rank_names[NUM_RANKS] = {
		[EASY] = "EASY", [MEDIUM] = "MEDIUM", [HARD] = "HARD",
	};

	const float xf = x * CELLSIZE;
	float yf = y * CELLSIZE - FONTSIZE;

	DrawTextEx(font, TextFormat("OPTIONS MENU"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("Press BCKSP to go back"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("Rank       < %-6s >", rank_names[stt_get_rank()]),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_LEVEL ? RED : WHITE);
	DrawTextEx(font, TextFormat("Walls      < %-6s >",
	                            stt_walls_are_active() ? "ON" : "OFF"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_WALLS ? RED : WHITE);
	DrawTextEx(font, TextFormat("Background < %-6s >",
                                background_option_active() ? "ON" : "OFF"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_BACKGROUND ? RED : WHITE);
	if (selected_background == num_backgrounds) {
		DrawTextEx(font, TextFormat("Background Select (%s)", "RANDOM"),
		           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
		           option == OPTION_CHOOSE_BACKGROUND ? RED : WHITE);
	}
	else {
		DrawTextEx(font, TextFormat("Background Select (%s)",
		                            background_get_name(selected_background)),
		           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
		           option == OPTION_CHOOSE_BACKGROUND ? RED : WHITE);
	}
	DrawTextEx(font, TextFormat("Video Options"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_VIDEO ? RED : WHITE);
	DrawTextEx(font, TextFormat("Sound Options"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_SOUND ? RED : WHITE);
	DrawTextEx(font, TextFormat("Reset High Scores"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           option == OPTION_RESET ? RED : WHITE);
}

static void choose_background_entry(void)
{
	pointed_background = background_selected();
}

static void choose_background_update(void)
{
	if (IsKeyPressed(KEY_LEFT)) {
		--pointed_background;
		if (pointed_background < 0) {
			pointed_background = num_backgrounds;
		}
		PlaySound(sounds[SOUND_MENU_VERT]);
	}
	else
	if (IsKeyPressed(KEY_RIGHT)) {
		++pointed_background;
		if (pointed_background > num_backgrounds) {
			pointed_background = 0;
		}
		PlaySound(sounds[SOUND_MENU_VERT]);
	}

	if (IsKeyPressed(KEY_OK)) {
		selected_background = pointed_background;
		background_change(selected_background);
		PlaySound(sounds[SOUND_MENU_HORI]);
	}
	else
	if (IsKeyPressed(KEY_GOBACK)) {
		change_options_state(STATE_OPTIONS);
		PlaySound(sounds[SOUND_MENU_BACK]);
	}

	background_update();
}

static void choose_background_render(void)
{
	int n;
	int n_total = 0;

	background_render();

	DrawTextEx(font, TextFormat("Background %n", &n),
	           (Vector2){0.0f, CELLSIZE * V_CELLS}, FONTSIZE, 0.0f, RED);
	n_total += n;

	if (pointed_background == num_backgrounds) {
		DrawTextEx(font, TextFormat("< %7s >   %n", "RANDOM", &n),
		           (Vector2){CELLSIZE * n_total, CELLSIZE * V_CELLS}, FONTSIZE,
		           0.0f, RED);
	}
	else {
		DrawTextEx(font, TextFormat("< %7s >   %n",
		                            background_get_name(pointed_background), &n),
		           (Vector2){CELLSIZE * n_total, CELLSIZE * V_CELLS}, FONTSIZE,
		           0.0f, RED);
	}
	n_total += n;

	if (selected_background == num_backgrounds) {
		DrawTextEx(font, TextFormat("%7s", "RANDOM"),
		           (Vector2){CELLSIZE * n_total, CELLSIZE * V_CELLS}, FONTSIZE,
		           0.0f, WHITE);
	}
	else {
		DrawTextEx(font, TextFormat("%7s",
		                            background_get_name(selected_background)),
		           (Vector2){CELLSIZE * n_total, CELLSIZE * V_CELLS}, FONTSIZE,
		           0.0f, WHITE);
	}
}

static void video_options_update(void)
{
	if (IsKeyPressed(KEY_GOBACK)) {
		change_options_state(STATE_OPTIONS);
		PlaySound(sounds[SOUND_MENU_BACK]);

		return;
	}

	if (IsKeyPressed(KEY_DOWN))  ++video_option;
	else
	if (IsKeyPressed(KEY_UP))  --video_option;

	if (video_option < 0)  video_option = 0;
	else
	if (video_option >= NUM_VIDEO_OPTS)  video_option = NUM_VIDEO_OPTS - 1;
	else
	if (IsKeyPressed(KEY_DOWN) || IsKeyPressed(KEY_UP)) {
		PlaySound(sounds[SOUND_MENU_VERT]);
	}

	switch (video_option) {
	case VOPT_SCALE:
		if (IsKeyPressed(KEY_RIGHT)) {
			int cur_scale = *video_scale;

			if (video_set_scale(cur_scale + 1) != cur_scale) {
				PlaySound(sounds[SOUND_MENU_HORI]);
			}
		}
		else
		if (IsKeyPressed(KEY_LEFT)) {
			int cur_scale = *video_scale;

			if (video_set_scale(cur_scale - 1) != cur_scale) {
				PlaySound(sounds[SOUND_MENU_HORI]);
			}
		}

		break;

	case VOPT_MODE:
		if (IsKeyPressed(KEY_OK)){
			PlaySound(sounds[SOUND_MENU_HORI]);
			video_toggle_fullscreen();
		}

		break;

	case VOPT_VSYNC:
		if (IsKeyPressed(KEY_LEFT) || IsKeyPressed(KEY_RIGHT) ||
		    IsKeyPressed(KEY_OK))
		{
			PlaySound(sounds[SOUND_MENU_HORI]);
			video_toggle_vsync();
		}

		break;

	default:
		break;
	}
}

static void video_options_render(void)
{
	video_options_text(0, 0);
}

static void video_options_text(const int x, const int y)
{
	const float xf = x * CELLSIZE;
	float yf = y * CELLSIZE - FONTSIZE;

	DrawTextEx(font, "VIDEO OPTIONS",
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("Size  < %2dX >", *video_scale),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           video_option == VOPT_SCALE ? RED : WHITE);
	DrawTextEx(font, TextFormat("Toggle Fullscreen"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           video_option == VOPT_MODE ? RED : WHITE);
	DrawTextEx(font, TextFormat("VSync < %3s >", *video_vsync ? "ON" : "OFF"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           video_option == VOPT_VSYNC ? RED : WHITE);
}

static void sound_options_entry(void)
{
	cur_music_volume = audio_get_music_user_volume();
	cur_sfx_volume = audio_get_sfx_user_volume();
}

static void sound_options_update(void)
{
	if (IsKeyPressed(KEY_GOBACK)) {
		change_options_state(STATE_OPTIONS);
		PlaySound(sounds[SOUND_MENU_BACK]);

		return;
	}

	if (IsKeyPressed(KEY_DOWN))  ++sound_option;
	else
	if (IsKeyPressed(KEY_UP))  --sound_option;

	if (sound_option < 0)  sound_option = 0;
	else
	if (sound_option >= NUM_SOUND_OPTS)  sound_option = NUM_SOUND_OPTS - 1;
	else
	if (IsKeyPressed(KEY_DOWN) || IsKeyPressed(KEY_UP)) {
		PlaySound(sounds[SOUND_MENU_VERT]);
	}

	switch (sound_option) {
	case SOPT_MUSIC_VOLUME:
		if (IsKeyPressed(KEY_RIGHT)) {
			cur_music_volume += MUSIC_VOL_VARIATION;
			cur_music_volume = audio_set_music_user_volume(cur_music_volume);
		}
		else
		if (IsKeyPressed(KEY_LEFT)) {
			cur_music_volume -= MUSIC_VOL_VARIATION;
			cur_music_volume = audio_set_music_user_volume(cur_music_volume);
		}

		break;

	case SOPT_SFX_VOLUME:
		if (IsKeyPressed(KEY_RIGHT)) {
			cur_sfx_volume += SFX_VOL_VARIATION;
			cur_sfx_volume = audio_set_sfx_user_volume(cur_sfx_volume);
		}
		else
		if (IsKeyPressed(KEY_LEFT)) {
			cur_sfx_volume -= SFX_VOL_VARIATION;
			cur_sfx_volume = audio_set_sfx_user_volume(cur_sfx_volume);
		}

		break;

	case SOPT_SOUNDTEST_MUSIC:
		if (IsKeyPressed(KEY_OK)) {
			audio_play_song(selected_song);
		}
		else
		if (IsKeyPressed(KEY_RIGHT)) {
			++selected_song;
			if (selected_song >= NUM_SONGS) {
				selected_song = 0;
			}
		}
		else
		if (IsKeyPressed(KEY_LEFT)) {
			if (selected_song == 0) {
				selected_song = NUM_SONGS - 1;
			}
			else {
				--selected_song;
			}
		}

		break;

	case SOPT_SOUNDTEST_SFX:
		if (IsKeyPressed(KEY_OK)) {
			PlaySound(sounds[selected_sound]);
		}
		else
		if (IsKeyPressed(KEY_RIGHT)) {
			++selected_sound;
			if (selected_sound >= NUM_SOUNDS) {
				selected_sound = 0;
			}
		}
		else
		if (IsKeyPressed(KEY_LEFT)) {
			if (selected_sound == 0) {
				selected_sound = NUM_SOUNDS - 1;
			}
			else {
				--selected_sound;
			}
		}

		break;

	default:
		break;
	}

	if (IsKeyPressed(KEY_RIGHT) || IsKeyPressed(KEY_LEFT)) {
		PlaySound(sounds[SOUND_MENU_HORI]);
	}
}

static void sound_options_render(void)
{
	sound_options_text(0, 0);
}

static void sound_options_text(const int x, const int y)
{
	const float xf = x * CELLSIZE;
	float yf = y * CELLSIZE - FONTSIZE;

	DrawTextEx(font, "SOUND OPTIONS",
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("Music Volume < %2d >", cur_music_volume),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           sound_option == SOPT_MUSIC_VOLUME ? RED : WHITE);
	DrawTextEx(font, TextFormat("Sound Volume < %2d >", cur_sfx_volume),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           sound_option == SOPT_SFX_VOLUME ? RED : WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("SOUND TEST"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("Song  < %2d >", selected_song),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           sound_option == SOPT_SOUNDTEST_MUSIC ? RED : WHITE);
	DrawTextEx(font, TextFormat("Sound < %2d >", selected_sound),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f,
	           sound_option == SOPT_SOUNDTEST_SFX ? RED : WHITE);
}