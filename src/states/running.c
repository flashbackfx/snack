/* running.c */

#define SCORE_DETAILS

#include "running.h"
#include "main_menu.h"
#include "../constants.h"
#include "../main.h"
#include "../mm.h"
#include "../audio.h"
#include "../settings.h"
#include "../background.h"
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <raylib.h>
//#ifdef SCORE_DETAILS
 #include <stdio.h>
//#endif


#define KEY_SPRINT KEY_A
#define KEY_DEC KEY_Z

#define INITIAL_COUNTDOWN 80
#define FINAL_COUNTDOWN 30
#define RENDER_SCORE_DURATION 91

#define MOV_TYPE 2
#define MORE_SPRINT_CHANCES  // Affects motion_handler function

#if (MOV_TYPE == 0)
 #define ACCELERATION 0.05f
 #define MAX_SPEED 3.0f
 #define MIN_SPEED 1.0f
 #define CRUISE_SPEED 1.5f
 #define SPRINT_DURATION 6

 #define MOD_EASY 0.8f
 #define MOD_HARD 1.4f

#elif (MOV_TYPE == 1)
 #define ACCELERATION 0.05f
 #define MAX_SPEED /*3.0f*/ 4.0f
 #define MIN_SPEED 1.0f
 #define CRUISE_SPEED /*1.5f*/ 2.0f
 #define SPRINT_DURATION 6

 #define MOD_EASY 0.8f
 #define MOD_HARD 1.4f

#elif (MOV_TYPE == 2)
 #define ACCELERATION /*0.05f*/ 0.04f
 #define MAX_SPEED 4.0f
 #define MIN_SPEED 1.0f
 #define CRUISE_SPEED 2.0f
 #define SPRINT_DURATION /*6*/ 5

 #define MOD_EASY /*0.8f*/ 0.75f
 #define MOD_HARD /*1.4f*/ 1.5f

#elif (MOV_TYPE == 3)  // To try with MORE_SPRINT_CHANCES
 #define ACCELERATION /*0.04f*/ 0.03f
 #define MAX_SPEED 4.0f
 #define MIN_SPEED 1.0f
 #define CRUISE_SPEED 2.0f
 #define SPRINT_DURATION /*6*/ 5

 #define MOD_EASY /*0.8f*/ 0.75f
 #define MOD_HARD /*1.4f*/ 1.5f

#endif

#define DFT_GAME_TIMER 30
#define TIMER_UP 2  // Amount added to timer when eating a fruit
#define TIMER_UP_ABOVE_DFT 1  // Amount added to timer when eating a fruit with the timer above DFT_GAME_TIMER

#define SCORE_SCALING 100

#define RANK_BASE_EASY (2 * SCORE_SCALING)
#define RANK_BASE_MEDIUM (3 * SCORE_SCALING)
#define RANK_BASE_HARD (4 * SCORE_SCALING)
#define SPRINT_RATIO 0.5f
#define SPRINT_BONUS_EASY (RANK_BASE_EASY * SPRINT_RATIO)
#define SPRINT_BONUS_MEDIUM (RANK_BASE_MEDIUM * SPRINT_RATIO)
#define SPRINT_BONUS_HARD (RANK_BASE_HARD * SPRINT_RATIO)
#define SLOW_PENALTY_EASY -(SPRINT_BONUS_EASY)
#define SLOW_PENALTY_MEDIUM -(SPRINT_BONUS_MEDIUM)
#define SLOW_PENALTY_HARD -(SPRINT_BONUS_HARD)
#define SPRINT_BONUS_SPEED (MAX_SPEED - (MAX_SPEED * 0.25f))  // For rank Medium
#define SLOW_PENALTY_SPEED (MIN_SPEED + (MIN_SPEED * 0.25f))  // For rank Medium
#define LENGTH_SCORE_RATIO 16
#define INITIAL_LEN 4
#define STARTING_FRUIT_STEP 60
#define DISTANCE_BONUS_RATIO 4
#define FRUIT_SCORE_SUB (1 * SCORE_SCALING)
#define WALL_MODIFIER 1.2f
#define FINAL_MOD_EASY 1.0f
#define FINAL_MOD_NORMAL 1.2f
#define FINAL_MOD_HARD 1.4f

#define EMPTY 0xFF  // Used in the snake.body array to indicate cells the snake is not occupying

#define FRUIT_SWAP_ANIM_SPD 0.5f

#define KEY_DBG_TIME_MORE KEY_M
#define KEY_DBG_TIME_LESS KEY_N
#define DBG_TIME_AMOUNT 10

#define MAX_RND_BG_REPETITION 3  // How many plays the same backgound is used when the random backgrounds option is choosen
#if (MAX_RND_BG_REPETITION < 0)
 #error MAX_RND_BG_REPETITION < 0
#endif


enum game_running_states {
	GR_INITIAL_COUNTDOWN,
	GR_GAMEPLAY,
	GR_GAME_END,
	GR_GAME_END_HSCORE,
	GR_PAUSE,
};

enum directions {
	RIGHT = 0x0,
	UP,
	LEFT,
	DOWN,
	NUM_DIRECTIONS,
	DIRECTION_NONE = NUM_DIRECTIONS,
};

enum mov_state {
	DECEL = -1,
	CRUISE = 0,
	SPRINT,
	TO_CRUISE,
};

enum sprite_tags {
	SNAKE_SLOW_HEAD,
	SNAKE_SLOW_PREHEAD,
	SNAKE_HEAD,
	SNAKE_PREHEAD,
	SNAKE_BODY,
	SNAKE_POSTTAIL,
	SNAKE_TAIL,
	SNAKE_SPRINT_HEAD,
	SNAKE_SPRINT_PREHEAD,
	SNAKE_SPRINT_BODY,
	SNAKE_SPRINT_POSTTAIL,
	SNAKE_SPRINT_TAIL,
	NUM_BASE_SNAKE_SPRITES,
	FRUIT_G = 0x18,  // FRUIT_G is manually aligned to spritesheet
	SNAKE_SPAWN = FRUIT_G + 0x03,  // SNAKE_SPAWN is manually aligned to spritesheet
	WALL = FRUIT_G + 0x04,  // WALL is manually aligned to spritesheet
	SCORE_GT_PANEL = 0xFD,
	SCORE_GT = 0xFE,
	RENDER_NONE = 0xFF,
	SPRINT_OFFSET = SNAKE_SPRINT_HEAD - SNAKE_HEAD,
};


static int rnd_bg_repetition = 0;  // Counts how many plays a background is used when the random background option is choosen
static int global_count;  // Used for the countdown states

static const float final_score_mods[NUM_RANKS] = {
	FINAL_MOD_EASY, FINAL_MOD_NORMAL, FINAL_MOD_HARD
};
static float final_score_mod;

static struct {
	int game;
	int time_bonus;
	int length_bonus;
	int final;
	char initials[NAME_SIZE];
} game_score;

static struct {
	int num_elements;
	struct render_table {
		signed char x;
		signed char y;
		unsigned char sprite;
		unsigned char step;
		unsigned char rotation;
		unsigned char flip_xy;
		unsigned char pad1, pad2;
	} table[V_CELLS * H_CELLS * 2];
} render;

static struct {
	float step;
	float speed;
	enum mov_state mov_state;
	unsigned char direction;
	unsigned char direction_candidate;
	unsigned char direction_candidate_post;
	unsigned char previous_direction;
	signed char head_x;
	signed char head_y;
	unsigned char tail_direction;
	signed char tail_x;
	signed char tail_y;
	char eating;
	char pad1, pad2;
	unsigned char body[V_CELLS][H_CELLS];  // Holds snake body. The lowest 2 bits represent direction and the next 2 the previous direction
} snake;

static struct {
	float acceleration;
	float max_speed;
	float min_speed;
	float cruise_speed;
	int sprint_duration;
} mov_info;

static struct {
	float animation_step;
	signed char x;
	signed char y;
	signed char prev_x;
	signed char prev_y;
	enum {FRUIT_LAY, FRUIT_SWAP} state;
} fruit;

static struct {
	int rank_base;
	float sprint;
	float sprint_bonus_spd;
	float slow;
	float slow_penalty_spd;
	int snake_len;
	int bonus;  // Time-distance bonus
	int step;   // Time-distance counter
	float wall_modifier;
	int render_score;  // Value of previous fruit score for rendering
	int render_score_frame;  // How long the score has been displayed
	int render_len;  // Visual width of the score in cells
} fruit_score;

static struct {
	int value;
	int cur_frame;
} game_timer;


static void game_running_entry(void);
static void change_gr_state(enum game_running_states new_grstate);

static void game_pause_entry(void);
static void game_pause_update(void);
static void game_pause_exit(void);

static void game_init(void);
static void fill_mov_info(enum rank difficulty);
static void fill_score_attributes(enum rank difficulty);
static void initial_countdown_update(void);
static void snake_countdown(int count);
static void initial_countdown_exit(void);

static void gameplay_entry(void);
static void gameplay_update(void);
static void update_fruit(void);
static void update_snake(void);
static void direction_handler(void);
static enum mov_state motion_handler(enum mov_state mstate);
static void snake_advance_cell(void);
static void update_timer(void);
static void put_fruit(void);
static int give_score(void);
static void game_end(void);
static void gameplay_exit(void);

static void game_end_entry(void);
static void game_end_update(void);
static void game_end_update_hscore(void);
static void game_end_exit(void);

static void render_table_add(signed char x, signed char y, unsigned char sprite,
                             unsigned char step, unsigned char rotation,
                             unsigned char flip_xy);
static void fill_render_table_countdown(void);
static void fill_render_table(void);
static void render_walls();
static void graphic_lut(enum directions direction,
                        enum directions previous_direction,
                        enum sprite_tags *sprite, unsigned char *rotation,
                        unsigned char *flip_y);
static void game_running_render(void);
static void game_running_render_countdown(void);
static void game_running_render_pause(void);
static void game_running_render_end(void);
static void game_end_message(const int x, const int y);
static void high_score_message(const int x, const int y);

static void game_running_exit(void);
static void show_score_macros(void);  // Debug


void game_running(void)
{
	change_gr_state(GR_INITIAL_COUNTDOWN);
	push_entry_action(game_running_entry);
}

static void game_running_entry(void)
{
	rnd_bg_repetition = 0;
}

static void change_gr_state(enum game_running_states new_grstate)
{
	switch (new_grstate) {
	case GR_INITIAL_COUNTDOWN:
		transition_substate(
			game_init,
			initial_countdown_update,
			game_running_render_countdown,
			initial_countdown_exit
		);

		break;

	case GR_GAMEPLAY:
		transition_substate(
			gameplay_entry,
			gameplay_update,
			game_running_render,
			gameplay_exit
		);

		break;

	case GR_GAME_END:
		transition_substate(
			game_end_entry,
			game_end_update,
			game_running_render_end,
			game_end_exit
		);

		break;

	case GR_GAME_END_HSCORE:
		transition_substate(
			game_end_entry,
			game_end_update_hscore,
			game_running_render_end,
			empty_exit_action
		);

		break;

	case GR_PAUSE:
		transition_substate(
			game_pause_entry,
			game_pause_update,
			game_running_render_pause,
			game_pause_exit
		);

		break;
	}
}

static void game_pause_entry(void)
{
	PlaySound(sounds[SOUND_MENU_ENTER]);
}

static void game_pause_update(void)
{
	if (IsKeyPressed(KEY_GOBACK)) {
		push_exit_action(game_running_exit);
		main_menu();
		return;
	}
	if (IsKeyPressed(KEY_OK)) {
		change_gr_state(GR_GAMEPLAY);
	}
}

static void game_pause_exit(void)
{
	PlaySound(sounds[SOUND_MENU_BACK]);
}

static void game_init(void)
{
	show_score_macros();  // Debug

	audio_play_song(SONG_NONE);

	global_count = 0;
	game_timer.value = DFT_GAME_TIMER;
	game_timer.cur_frame = 0;
	game_score.game = 0;
	game_score.time_bonus = 0;
	game_score.length_bonus = 0;
	game_score.final = 0;
	memset(&game_score.initials, '\0', sizeof(game_score.initials));
	game_score.initials[0] = 'A';  // 'A' is the first entry of the static const char selectable[] array of game_end_hscore_update

	fill_mov_info(stt_get_rank());
	fill_score_attributes(stt_get_rank());

	final_score_mod = final_score_mods[stt_get_rank()];

	for (int y = 0; y < V_CELLS; ++y) {
		for (int x = 0; x < H_CELLS; ++x) {
			snake.body[y][x] = EMPTY;
		}
	}

	snake.tail_x = (H_CELLS / 2) - (INITIAL_LEN / 2) - 1;
	snake.tail_y = (V_CELLS / 2);
	snake.direction = RIGHT;
	snake.direction_candidate = snake.direction;
	snake.direction_candidate_post = DIRECTION_NONE;
	snake.previous_direction = snake.direction;
	snake.tail_direction = snake.direction;
	snake.step = 0.0f;
	snake.speed = mov_info.cruise_speed;
	snake.mov_state = CRUISE;
	snake.eating = 0;

	int i;

	for (i = 1; i <= INITIAL_LEN; ++i) {
		snake.body[snake.tail_y][snake.tail_x + i] = snake.direction;
	}
	snake.head_x = snake.tail_x + i;
	snake.head_y = snake.tail_y;

	fruit.x = fruit.y = -1;  // Trick for hiding fruit swapping when starting a new game
}

static void fill_mov_info(enum rank difficulty)
{
	switch (difficulty) {
	case EASY:
		mov_info.acceleration = ACCELERATION * MOD_EASY;
		mov_info.max_speed = MAX_SPEED * MOD_EASY;
		mov_info.min_speed = MIN_SPEED * MOD_EASY;
		mov_info.cruise_speed = CRUISE_SPEED * MOD_EASY;
		mov_info.sprint_duration = SPRINT_DURATION /** MOD_EASY*/;
		break;

	case MEDIUM:
		mov_info.acceleration = ACCELERATION;
		mov_info.max_speed = MAX_SPEED;
		mov_info.min_speed = MIN_SPEED;
		mov_info.cruise_speed = CRUISE_SPEED;
		mov_info.sprint_duration = SPRINT_DURATION;
		break;

	case HARD:
		mov_info.acceleration = ACCELERATION * MOD_HARD;
		mov_info.max_speed = MAX_SPEED * MOD_HARD;
		mov_info.min_speed = MIN_SPEED * MOD_HARD;
		mov_info.cruise_speed = CRUISE_SPEED * MOD_HARD;
		mov_info.sprint_duration = SPRINT_DURATION /** MOD_HARD*/;
		break;

	default:
		TraceLog(LOG_WARNING, TextFormat("%s: Wrong case %d", __func__, difficulty));
		break;
	}
}

static void fill_score_attributes(enum rank difficulty)
{
	switch (difficulty) {
	case EASY:
		fruit_score.rank_base = RANK_BASE_EASY;
		fruit_score.sprint = SPRINT_BONUS_EASY;
		fruit_score.slow = SLOW_PENALTY_EASY;
		fruit_score.sprint_bonus_spd = SPRINT_BONUS_SPEED * MOD_EASY;
		fruit_score.slow_penalty_spd = SLOW_PENALTY_SPEED * MOD_EASY;
		break;

	case MEDIUM:
		fruit_score.rank_base = RANK_BASE_MEDIUM;
		fruit_score.sprint = SPRINT_BONUS_MEDIUM;
		fruit_score.slow = SLOW_PENALTY_MEDIUM;
		fruit_score.sprint_bonus_spd = SPRINT_BONUS_SPEED;
		fruit_score.slow_penalty_spd = SLOW_PENALTY_SPEED;
		break;

	case HARD:
		fruit_score.rank_base = RANK_BASE_HARD;
		fruit_score.sprint = SPRINT_BONUS_HARD;
		fruit_score.slow = SLOW_PENALTY_HARD;
		fruit_score.sprint_bonus_spd = SPRINT_BONUS_SPEED * MOD_HARD;
		fruit_score.slow_penalty_spd = SLOW_PENALTY_SPEED * MOD_HARD;
		break;

	default:
		TraceLog(LOG_WARNING, TextFormat("%s: Wrong case %d", __func__, difficulty));
		break;
	}

	if (stt_walls_are_active()) {
		fruit_score.wall_modifier = WALL_MODIFIER;
	}
	else {
		fruit_score.wall_modifier = 1.0f;
	}

	fruit_score.snake_len = INITIAL_LEN;  // May be moved to snake struct
	fruit_score.bonus = 0;
	fruit_score.step = STARTING_FRUIT_STEP;
	fruit_score.render_score = 0;
	fruit_score.render_score_frame = 0;
}

static void initial_countdown_update(void)
{
	++global_count;

	if (global_count == 1) {
		PlaySound(sounds[SOUND_READY]);
	}
	else
	if (global_count == (INITIAL_COUNTDOWN / 2) + 1) {
		PlaySound(sounds[SOUND_GO]);
	}
	else
	if (global_count >= INITIAL_COUNTDOWN) {
		change_gr_state(GR_GAMEPLAY);
	}

	snake_countdown(global_count - 1);

	fill_render_table_countdown();
	background_update();
}

static void snake_countdown(int count)
{
	if (snake.step <= CELLSIZE) {
		snake.step = (count / 2) % CELLSIZE_I;
	}
}

static void initial_countdown_exit(void)
{
	global_count = 0;
	snake.step = 0.0f;

	put_fruit();

	audio_play_song(GetRandomValue(SONG_GAMEPLAY_1, SONG_GAMEPLAY_2));
}

static void gameplay_entry(void)
{
}

static void gameplay_update(void)
{
	background_update();

	update_timer();
	update_fruit();
	update_snake();

	fill_render_table();

	if (IsKeyPressed(KEY_OK)) {
		change_gr_state(GR_PAUSE);
	}
}

static void update_fruit(void)
{
	if (fruit_score.bonus > 0) {
		--fruit_score.step;

		if (fruit_score.step == 0) {
			fruit_score.bonus -= FRUIT_SCORE_SUB;
			fruit_score.step = STARTING_FRUIT_STEP;

			if (fruit_score.bonus < 0) {
				fruit_score.bonus = 0;
			}
		}
	}

	if (fruit_score.render_score_frame > 0) {
		++fruit_score.render_score_frame;

		if (fruit_score.render_score_frame > RENDER_SCORE_DURATION) {
			fruit_score.render_score_frame = 0;
		}
	}

	if (fruit.state == FRUIT_LAY) {
		fruit.animation_step += (1.0f + (fruit_score.bonus * (0.2f / SCORE_SCALING))) * 0.5f;  // SCORE_SCALING should go and a FRUIT_ANIM_SPEED_SCALING, or something, value should be used instead (?) (FRUIT_ANIM_SPEED_SCALING could still be based in SCORE_SCALING tho)

		if (fruit.animation_step >= CELLSIZE) {
			fruit.animation_step -= CELLSIZE;
		}
	}
	else {  // fruit.state == FRUIT_SWAP
		fruit.animation_step += FRUIT_SWAP_ANIM_SPD;

		if (fruit.animation_step >= CELLSIZE) {
			fruit.animation_step = 0.0f;
			fruit.state = FRUIT_LAY;
		}
	}
}

static void update_snake(void)
{
	direction_handler();
	snake.mov_state = motion_handler(snake.mov_state);

	snake.step += snake.speed;

	if (snake.step >= CELLSIZE) {
		snake_advance_cell();
		snake.step -= CELLSIZE;
	}
}

static void direction_handler(void)
{
	// NOTE: Uncomment block in snake_advance_cell for proper printf debugging

	switch (snake.direction) {
	case LEFT:
	case RIGHT:
		if (IsKeyPressed(KEY_UP)) {
			snake.direction_candidate = UP;
			fprintf(stderr, "%lld - %s--: dc  UP\n", cur_frame, __func__);
		}
		else
		if (IsKeyPressed(KEY_DOWN)) {
			snake.direction_candidate = DOWN;
			fprintf(stderr, "%lld - %s--: dc  DOWN\n", cur_frame, __func__);
		}

		if (IsKeyPressed(KEY_LEFT)) {
			snake.direction_candidate_post = LEFT;
			fprintf(stderr, "%lld - %s--: dcp LEFT\n", cur_frame, __func__);
		}
		else
		if (IsKeyPressed(KEY_RIGHT)) {
			snake.direction_candidate_post = RIGHT;
			fprintf(stderr, "%lld - %s--: dcp RIGHT\n", cur_frame, __func__);
		}

		break;

	case UP:
	case DOWN:
		if (IsKeyPressed(KEY_LEFT)) {
			snake.direction_candidate = LEFT;
			fprintf(stderr, "%lld - %s--: dc  LEFT\n", cur_frame, __func__);
		}
		else
		if (IsKeyPressed(KEY_RIGHT)) {
			snake.direction_candidate = RIGHT;
			fprintf(stderr, "%lld - %s--: dc  RIGHT\n", cur_frame, __func__);
		}

		if (IsKeyPressed(KEY_UP)) {
			snake.direction_candidate_post = UP;
			fprintf(stderr, "%lld - %s--: dcp UP\n", cur_frame, __func__);
		}
		else
		if (IsKeyPressed(KEY_DOWN)) {
			snake.direction_candidate_post = DOWN;
			fprintf(stderr, "%lld - %s--: dcp DOWN\n", cur_frame, __func__);
		}

		break;

	default:  break;
	}
}

static enum mov_state motion_handler(enum mov_state mstate)
{
	static int sprint_counter = 0;
#ifdef MORE_SPRINT_CHANCES
	static bool sprint_ready_notified = true;
#endif

	switch (mstate) {
	case CRUISE:
		if (IsKeyPressed(KEY_SPRINT)) {
			snake.speed = mov_info.max_speed;
			sprint_counter = mov_info.sprint_duration;
			PlaySound(sounds[SOUND_SPRINT_ACTIVE]);
		#ifdef MORE_SPRINT_CHANCES
			sprint_ready_notified = false;
		#endif
			return SPRINT;
		}
		if (IsKeyDown(KEY_DEC)) {
		#ifdef MORE_SPRINT_CHANCES
			sprint_ready_notified = false;
		#endif
			return DECEL;
		}
		break;

	case SPRINT:
		if (--sprint_counter == 0) {
			return TO_CRUISE;
		}
		if (IsKeyDown(KEY_DEC)) {
			sprint_counter = 0;
			return DECEL;
		}
		break;

	case DECEL:
		if (!IsKeyDown(KEY_DEC)) {
			return TO_CRUISE;
		}
		snake.speed -= mov_info.acceleration;
		if (snake.speed < mov_info.min_speed) {
			snake.speed = mov_info.min_speed;
		}
		break;

	case TO_CRUISE:
		if (IsKeyDown(KEY_DEC)) {
			return DECEL;
		}
	#ifdef MORE_SPRINT_CHANCES
		if (snake.speed > fruit_score.slow_penalty_spd &&
		    snake.speed < fruit_score.sprint_bonus_spd)
		{
			if (sprint_ready_notified == false) {
				PlaySound(sounds[SOUND_SPRINT_READY]);
				sprint_ready_notified = true;
			}
			if (IsKeyPressed(KEY_SPRINT)) {
				snake.speed = mov_info.max_speed;
				sprint_counter = mov_info.sprint_duration;
				PlaySound(sounds[SOUND_SPRINT_ACTIVE]);
				sprint_ready_notified = false;
				return SPRINT;
			}
		}
	#endif
		if (snake.speed >= mov_info.cruise_speed - mov_info.acceleration &&
		    snake.speed <= mov_info.cruise_speed + mov_info.acceleration)
		{
			snake.speed = mov_info.cruise_speed;
		#ifndef MORE_SPRINT_CHANCES
			PlaySound(sounds[SOUND_SPRINT_READY]);
		#endif
			return CRUISE;
		}

		if (snake.speed < mov_info.cruise_speed) {
			snake.speed += mov_info.acceleration;
		}
		else
		if (snake.speed > mov_info.cruise_speed) {
			snake.speed -= mov_info.acceleration;
		}

		break;

	default:
		break;
	}

	return mstate;
}

static void snake_advance_cell(void)
{
	static bool dcp_hold = false;  // true when one direction_candidate_post is being held (they can during 1 cell) NOTE: It sould maybe be in the snake struct so it can be zeroed when starting a play

#if 0
	// Movement debug messages
	if (snake.direction != snake.direction_candidate) {
		fprintf(stderr, "%lld %s: dc  %s\n", cur_frame, __func__, snake.direction_candidate == RIGHT ? "RIGHT" :
		                                                          snake.direction_candidate == UP ? "UP" :
		                                                          snake.direction_candidate == LEFT ? "LEFT" :
		                                                          snake.direction_candidate == DOWN ? "DOWN" : "N/A");
		fprintf(stderr, "%lld %s: dcp %s\n", cur_frame, __func__, snake.direction_candidate_post == RIGHT ? "RIGHT" :
		                                                          snake.direction_candidate_post == UP ? "UP" :
		                                                          snake.direction_candidate_post == LEFT ? "LEFT" :
		                                                          snake.direction_candidate_post == DOWN ? "DOWN" : "N/A");
		fprintf(stderr, "%lld dcp_hold %d\n", cur_frame, dcp_hold);
		fprintf(stderr, "---\n");
	}
	else {
		fprintf(stderr, "%lld -- %s: direction %s\n", cur_frame, __func__, snake.direction == RIGHT ? "RIGHT" :
		                                                                   snake.direction == UP ? "UP" :
		                                                                   snake.direction == LEFT ? "LEFT" :
		                                                                   snake.direction == DOWN ? "DOWN" : "N/A");
		fprintf(stderr, "%lld dcp_hold %d\n", cur_frame, dcp_hold);
	}
#endif

	snake.eating = 0;

	if (snake.direction_candidate_post != DIRECTION_NONE &&
	    snake.direction_candidate == snake.direction)
	{
		if (dcp_hold == false) {
			dcp_hold = true;
			fprintf(stderr, "%lld %s: --- dcp_hold --- ON\n", cur_frame, __func__);
		}
		else {
			snake.direction_candidate_post = DIRECTION_NONE;
			dcp_hold = false;
			fprintf(stderr, "%lld %s: --- dcp_hold --- OFF\n", cur_frame, __func__);
		}
	}

	snake.previous_direction = snake.direction;
	snake.direction = snake.direction_candidate;
	snake.body[snake.head_y][snake.head_x] = (snake.previous_direction << 2) | snake.direction;

	if (snake.direction_candidate_post != DIRECTION_NONE &&
	    snake.direction != snake.previous_direction)
	{
		snake.direction_candidate = snake.direction_candidate_post;
		snake.direction_candidate_post = DIRECTION_NONE;
		dcp_hold = false;
	}

	switch (snake.direction) {
	case LEFT:   --snake.head_x;  break;
	case RIGHT:  ++snake.head_x;  break;
	case UP:     --snake.head_y;  break;
	case DOWN:   ++snake.head_y;  break;
	default:                      break;
	}

	if (stt_walls_are_active()) {
		if (snake.head_x < 0 || snake.head_x >= H_CELLS ||
		    snake.head_y < 0 || snake.head_y >= V_CELLS)
		{
			PlaySound(sounds[SOUND_SNAKE_COLLISION]);
			//Debug line
			fprintf(stderr, "collision with wall\n");
			game_end();

			return;
		}
	}
	else {
		if (snake.head_x < 0)  snake.head_x = H_CELLS - 1;
		if (snake.head_y < 0)  snake.head_y = V_CELLS - 1;
		if (snake.head_x >= H_CELLS)  snake.head_x = 0;
		if (snake.head_y >= V_CELLS)  snake.head_y = 0;
	}

	if (snake.head_x == fruit.x && snake.head_y == fruit.y) {
		PlaySound(sounds[SOUND_EAT_FRUIT]);
		snake.eating = 1;
		game_score.game += give_score();
		++fruit_score.snake_len;
		put_fruit();

		if (game_timer.value < DFT_GAME_TIMER) {
			game_timer.value += TIMER_UP;
		}
		else {
			game_timer.value += TIMER_UP_ABOVE_DFT;
		}
	}
	else
	if (snake.body[snake.head_y][snake.head_x] != EMPTY) {
		PlaySound(sounds[SOUND_SNAKE_COLLISION]);
		// Debug line
		fprintf(stderr, "collision with snake\n");
		game_end();

		return;
	}
	else {
		switch (snake.tail_direction) {
		case LEFT:   --snake.tail_x;  break;
		case RIGHT:  ++snake.tail_x;  break;
		case UP:     --snake.tail_y;  break;
		case DOWN:   ++snake.tail_y;  break;
		default:                      break;
		}

		if (stt_walls_are_active() == false) {
			if (snake.tail_x < 0)  snake.tail_x = H_CELLS - 1;
			if (snake.tail_y < 0)  snake.tail_y = V_CELLS - 1;
			if (snake.tail_x >= H_CELLS)  snake.tail_x = 0;
			if (snake.tail_y >= V_CELLS)  snake.tail_y = 0;
		}

		snake.tail_direction = snake.body[snake.tail_y][snake.tail_x] & 0x03;
		snake.body[snake.tail_y][snake.tail_x] = EMPTY;
	}
}

static void update_timer(void)
{
	// Debug
	{
		if (IsKeyPressed(KEY_DBG_TIME_MORE)) {
			game_timer.value += DBG_TIME_AMOUNT;
			game_timer.cur_frame = 0;
		}
		else
		if (IsKeyPressed(KEY_DBG_TIME_LESS)) {
			game_timer.value -= DBG_TIME_AMOUNT;
			game_timer.cur_frame = 0;
		}
	}

	++game_timer.cur_frame;

	if (game_timer.cur_frame >= DFT_FPS) {
		game_timer.cur_frame = 0;
		--game_timer.value;
		++game_score.time_bonus;

		if (game_timer.value <= 0) {
			PlaySound(sounds[SOUND_TIME_OUT]);
			game_end();
		}
	}
}

static void put_fruit(void)
{
	static ptrdiff_t available_positions[V_CELLS * H_CELLS];
	int count = 0;

	// Select an empty position in the playfield

	for (int i = 0; i < V_CELLS * H_CELLS; ++i) {
		if (*(snake.body[0] + i) == EMPTY) {
			available_positions[count++] = i;
		}
	}

	if (count <= 2) {  // Take snake head and tail into account when determining it there's space left in the playfield
		game_end();

		return;
	}

	int x;
	int y;

	do {
		int n = available_positions[GetRandomValue(0, count - 1)];

		y = n / H_CELLS;
		x = n % H_CELLS;
	} while ((y == snake.head_y && x == snake.head_x) ||
	         (y == snake.tail_y && x == snake.tail_x));

	// Update fruit values

	fruit.state = FRUIT_SWAP;
	fruit.prev_x = fruit.x;
	fruit.prev_y = fruit.y;
	fruit.x = x;
	fruit.y = y;
	fruit.animation_step = 0.0f;
	fruit_score.step = STARTING_FRUIT_STEP;
	fruit_score.render_score_frame = 1;

	// Compute new distance-time bonus

	if (stt_walls_are_active())	{
		fruit_score.bonus = (abs(snake.head_x - x) + abs(snake.head_y - y)) / DISTANCE_BONUS_RATIO;
	}
	else {
		int distx = abs(snake.head_x - x);
		int disty = abs(snake.head_y - y);

		if (distx > H_CELLS / 2) {
			distx = H_CELLS - distx;
		}
		if (disty > V_CELLS / 2) {
			disty = V_CELLS - disty;
		}

		fruit_score.bonus = (distx + disty) / DISTANCE_BONUS_RATIO;
	}

	fruit_score.bonus *= SCORE_SCALING;

#ifdef SCORE_DETAILS
	TraceLog(LOG_INFO, TextFormat("fr  x %2d  fr  y %2d", x, y));
	TraceLog(LOG_INFO, TextFormat("headx %2d  heady %2d, bonus is %d", snake.head_x, snake.head_y, fruit_score.bonus));
#endif
}

static int give_score(void)
{
	float score = 0.0f;

	score += fruit_score.rank_base;  // Rank base
	score += fruit_score.bonus;  // Distance-time bonus

	// Speed
	if (snake.speed >= fruit_score.sprint_bonus_spd) {
		score += fruit_score.sprint;  // Sprint bonus
	}
	else
	if (snake.speed <= fruit_score.slow_penalty_spd) {
		score += fruit_score.slow;  // Slow penalty
	}

	score += (fruit_score.snake_len / LENGTH_SCORE_RATIO) * SCORE_SCALING;  // Snake length bonus (Integer division)
	score *= fruit_score.wall_modifier;  // Wall bonus

	fruit_score.render_score = (int) score;

	const Vector2 render_size = MeasureTextEx(font, TextFormat("+%d", fruit_score.render_score), 8.0f, 0.0f);
	fruit_score.render_len = render_size.x / CELLSIZE;

#ifdef SCORE_DETAILS
	// Watch out for changes!
	float s;
	float sc = 0.0f;

	printf("score:\n");

	s = fruit_score.rank_base;
	sc += s;
	printf("(rank +%.2f) %.2f ", s, sc);

	s = fruit_score.bonus;
	sc += s;
	printf("(dstime +%.2f) %.2f ", s, sc);

	s = 0.0f;
	if (snake.speed >= fruit_score.sprint_bonus_spd) {
		s = fruit_score.sprint;
		sc += s;
	}
	else
	if (snake.speed <= fruit_score.slow_penalty_spd) {
		s = fruit_score.slow;
		sc += s;
	}
	printf("(spd %+.2f) %.2f ", s, sc);

	s = (fruit_score.snake_len / LENGTH_SCORE_RATIO) * SCORE_SCALING;
	sc += s;
	printf("(len +%.2f) %.2f ", s, sc);

	s = fruit_score.wall_modifier;
	sc *= s;
	printf("(wall *%.2f) %.2f\n", s, sc);

	printf("Got %d (%d) points!\n", (int) sc, (int) score);
#endif

	return (int) score;
}

static void game_end(void)
{
	game_score.time_bonus *= SCORE_SCALING;
	game_score.length_bonus = (fruit_score.snake_len) * SCORE_SCALING * 2;  // Doubled so time_bonus and length_bonus tend to be closer

	game_score.final = (game_score.game + game_score.time_bonus + game_score.length_bonus) *
	                    final_score_mod;

#ifdef SCORE_DETAILS
	printf("final score:\n"
	       "((score) %d + (time_bonus) %d + (length_bonus) %d) * (rank_mod) %.2f = %d\n",
	       game_score.game, game_score.time_bonus, game_score.length_bonus,
	       final_score_mod, game_score.final);
#endif

	struct score mode_hscore = stt_get_mode_hi_score();

	if (game_score.final > mode_hscore.score) {
		change_gr_state(GR_GAME_END_HSCORE);
	}
	else {
		change_gr_state(GR_GAME_END);
	}
}

static void gameplay_exit(void)
{
}

static void game_end_entry(void)
{
	audio_play_song(SONG_NONE);
}

static void game_end_update(void)
{
	//whichfunc(__func__);
	background_update_colorcycle();

	if (global_count < FINAL_COUNTDOWN) {
		++global_count;
		snake_countdown(global_count - 1);
		fill_render_table_countdown();

		return;
	}

	if (IsKeyPressed(KEY_OK)) {
		// Play again
		change_gr_state(GR_INITIAL_COUNTDOWN);
	}
	else
	if (IsKeyPressed(KEY_GOBACK)) {
		// Back to menu
		PlaySound(sounds[SOUND_MENU_BACK]);
		push_exit_action(game_running_exit);
		main_menu();
	}
}

static void game_end_update_hscore(void)
{
	background_update_colorcycle();

	if (global_count < FINAL_COUNTDOWN) {
		++global_count;
		snake_countdown(global_count - 1);
		fill_render_table_countdown();

		return;
	}

	static const char selectable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!-+&*";
	static unsigned selected_char = 0;
	static int current_slot = 0;

	if (IsKeyPressed(KEY_UP)) {
		if (selected_char <= 0) {
			selected_char = sizeof(selectable) - 2;
		}
		else {
			--selected_char;
		}

		game_score.initials[current_slot] = selectable[selected_char];
		PlaySound(sounds[SOUND_MENU_HORI]);
	}
	else
	if (IsKeyPressed(KEY_DOWN)) {
		if (selected_char >= sizeof(selectable) - 2) {
			selected_char = 0;
		}
		else {
			++selected_char;
		}

		game_score.initials[current_slot] = selectable[selected_char];
		PlaySound(sounds[SOUND_MENU_HORI]);
	}
	else
	if (IsKeyPressed(KEY_SPRINT) || IsKeyPressed(KEY_OK)) {
		if (current_slot == sizeof(game_score.initials) - 2) {
			// Accept entered initials
			selected_char = 0;
			current_slot = 0;
			PlaySound(sounds[SOUND_EAT_FRUIT]);
			change_gr_state(GR_GAME_END);
		}
		else {
			++current_slot;
			game_score.initials[current_slot] = selectable[selected_char];
			PlaySound(sounds[SOUND_MENU_ENTER]);
		}
	}
	else
	if ((IsKeyPressed(KEY_DEC) || IsKeyPressed(KEY_GOBACK)) && current_slot > 0)
	{
		game_score.initials[current_slot] = '\0';
		--current_slot;
		//game_score.initials[current_slot] = selectable[selected_char];  // ...
		char *p = strchr(selectable, game_score.initials[current_slot]);
		selected_char = p - selectable;
		PlaySound(sounds[SOUND_MENU_BACK]);
	}
}

static void game_end_exit(void)
{
	const struct score mode_hscore = stt_get_mode_hi_score();

	if (game_score.final > mode_hscore.score) {
		stt_set_mode_hi_score(game_score.final, game_score.initials);
	}

	if (background_selection_is_random()) {
		++rnd_bg_repetition;

		if (rnd_bg_repetition >= MAX_RND_BG_REPETITION) {
			rnd_bg_repetition = 0;
			background_set();
		}
	}
}

static void render_table_add(signed char x, signed char y, unsigned char sprite,
                             unsigned char step, unsigned char rotation,
                             unsigned char flip_xy)
{
	struct render_table *cur = &(render.table[render.num_elements]);

	cur->x = x;
	cur->y = y;
	cur->sprite = sprite;
	cur->step = step;
	cur->rotation = rotation;
	cur->flip_xy = flip_xy;

	++render.num_elements;
}

static void fill_render_table_countdown(void)
{
	render.num_elements = 0;

	// Render walls
	if (stt_walls_are_active()) {
		render_walls();
	}

	// Render snake body
	for (int y = 0; y < V_CELLS; ++y) {
		for (int x = 0; x < H_CELLS; ++x) {
			if (snake.body[y][x] == EMPTY) {
				continue;
			}

			render_table_add(x, y, SNAKE_SPAWN, (unsigned char) snake.step, 0, 0);
		}
	}

	// Render snake head
	render_table_add(snake.head_x, snake.head_y, SNAKE_SPAWN,
	                 (unsigned char) snake.step, 0, 0);

	// Render snake tail
	render_table_add(snake.tail_x, snake.tail_y, SNAKE_SPAWN,
	                 (unsigned char) snake.step, 0, 0);
}

//#define BLINK
//#define BLINK_FAST
static void fill_render_table(void)
{
	int x;
	int y;
	enum sprite_tags lut_sprite;
	unsigned char lut_flip_y;
	unsigned char lut_rotation;
	static int blink;

	render.num_elements = 0;

	// Render walls
	if (stt_walls_are_active()) {
		render_walls();
	}

	// Render score back-panel
	if (fruit_score.render_score_frame > 0) {
		if (fruit.prev_x > H_CELLS - fruit_score.render_len) {
			x = H_CELLS - fruit_score.render_len;
		}
		else {
			x = fruit.prev_x;
		}
		if (fruit.prev_y == 0) {
			y = 1;
		}
		else {
			y = fruit.prev_y - 1;
		}

		render_table_add(x, y, SCORE_GT_PANEL, 0, 0, 0);
	}

	// Render snake start
	if (snake.speed >= fruit_score.sprint_bonus_spd) {
	#if defined(BLINK)
		static int blink_step;

		++blink_step;
		if (blink_step == 2) {
			blink = SPRINT_OFFSET - blink;
			blink_step = 0;
		}
	#elif defined(BLINK_FAST)
		blink = SPRINT_OFFSET - blink;
	#else
		blink = SPRINT_OFFSET;
	#endif
	}
	else {
		blink = 0;
	}

	// Render snake head
	if (snake.speed <= fruit_score.slow_penalty_spd) {
		lut_sprite = SNAKE_SLOW_HEAD;
	}
	else {
		lut_sprite = SNAKE_HEAD + blink;
	}

	graphic_lut(snake.direction, snake.previous_direction, &lut_sprite,
	            &lut_rotation, &lut_flip_y);

	render_table_add(snake.head_x, snake.head_y, lut_sprite,
	                 (unsigned char) snake.step, lut_rotation, lut_flip_y);

	// Render snake pre-head
	int prehead_x = snake.head_x;
	int prehead_y = snake.head_y;

	if (snake.direction == RIGHT)  prehead_x -= 1;
	else
	if (snake.direction == LEFT)  prehead_x += 1;
	else
	if (snake.direction == UP)  prehead_y += 1;
	else
	if (snake.direction == DOWN)  prehead_y -= 1;

	if (prehead_x < 0)  prehead_x += H_CELLS;
	else
	if (prehead_x >= H_CELLS)  prehead_x -= H_CELLS;

	if (prehead_y < 0)  prehead_y += V_CELLS;
	else
	if (prehead_y >= V_CELLS)  prehead_y -= V_CELLS;

	if (snake.speed <= fruit_score.slow_penalty_spd) {
		lut_sprite = SNAKE_SLOW_PREHEAD;
	}
	else {
		lut_sprite = SNAKE_PREHEAD + blink;
	}

	graphic_lut(snake.body[prehead_y][prehead_x] & 0x03,
	            snake.body[prehead_y][prehead_x] >> 2, &lut_sprite, &lut_rotation,
	            &lut_flip_y);

	render_table_add(prehead_x, prehead_y, lut_sprite,
	                 (unsigned char) snake.step, lut_rotation, lut_flip_y);

	// Render snake post-tail
	int postail_x = snake.tail_x;
	int postail_y = snake.tail_y;

	if (snake.tail_direction == RIGHT)  postail_x += 1;
	else
	if (snake.tail_direction == LEFT)  postail_x -= 1;
	else
	if (snake.tail_direction == UP)  postail_y -= 1;
	else
	if (snake.tail_direction == DOWN)  postail_y += 1;

	if (postail_x < 0)  postail_x += H_CELLS;
	else
	if (postail_x >= H_CELLS)  postail_x -= H_CELLS;

	if (postail_y < 0)  postail_y += V_CELLS;
	else
	if (postail_y >= V_CELLS)  postail_y -= V_CELLS;

	lut_sprite = SNAKE_POSTTAIL + blink;
	graphic_lut(snake.body[postail_y][postail_x] & 0x03,
	            snake.body[postail_y][postail_x] >> 2, &lut_sprite,
	            &lut_rotation, &lut_flip_y);

	render_table_add(postail_x, postail_y, lut_sprite,
	                 snake.eating ? (unsigned char) CELLSIZE_I - 1 :
	                                (unsigned char) snake.step,
	                 lut_rotation, lut_flip_y);

	// Render snake tail
	lut_sprite = SNAKE_TAIL + blink;
	graphic_lut(snake.tail_direction, snake.body[postail_y][postail_x] >> 2,
	            &lut_sprite, &lut_rotation, &lut_flip_y);  // The tail renders the animation for the next direction instead of the previous or current one

	render_table_add(snake.tail_x, snake.tail_y, lut_sprite,
	                 snake.eating ? (unsigned char) CELLSIZE_I - 1 :
	                                (unsigned char) snake.step,
	                 lut_rotation, lut_flip_y);

	// Render snake body
	for (y = 0; y < V_CELLS; ++y) {
		for (x = 0; x < H_CELLS; ++x) {
			if (snake.body[y][x] == EMPTY) {
				continue;
			}

			if ((x == prehead_x && y == prehead_y) ||
			    (x == postail_x && y == postail_y))
			{
				continue;
			}

			lut_sprite = SNAKE_BODY + blink;
			graphic_lut(snake.body[y][x] & 0x03, snake.body[y][x] >> 2,
			            &lut_sprite, &lut_rotation, &lut_flip_y);

			render_table_add(x, y, lut_sprite, (unsigned char) snake.step,
			                 lut_rotation, lut_flip_y);
		}
	}

	// Render fruit
	if (fruit.state == FRUIT_LAY) {
		render_table_add(fruit.x, fruit.y, FRUIT_G,
		                 (unsigned char) fruit.animation_step, 0, 0);
	}
	else {  // fruit.state == FRUIT_SWAP
		// Appearing fruit
		render_table_add(fruit.x, fruit.y, FRUIT_G + 1,
		                 (unsigned char) fruit.animation_step, 0, 0);  // Manual sprite selection

		// Eating fruit
		render_table_add(fruit.prev_x, fruit.prev_y, FRUIT_G + 2,
		                 (unsigned char) fruit.animation_step, 0, 0);  // Manual sprite selection
	}

	// Render score
	if (fruit_score.render_score_frame > 0) {
		if (fruit.prev_x > H_CELLS - fruit_score.render_len) {
			x = H_CELLS - fruit_score.render_len;
		}
		else {
			x = fruit.prev_x;
		}
		if (fruit.prev_y == 0) {
			y = 1;
		}
		else {
			y = fruit.prev_y - 1;
		}

		render_table_add(x, y, SCORE_GT, 0, 0, 0);
	}
}

static void render_walls()
{
	render_table_add(0, 0, WALL, 0, 0, 0);
	for (int i = 1; i < H_CELLS - 1; ++i) {
		render_table_add(i, 0, WALL, 1, 0, 0);
	}
	render_table_add(H_CELLS - 1, 0, WALL, 2, 0, 0);
	for (int i = 1; i < V_CELLS - 1; ++i) {
		render_table_add(H_CELLS - 1, i, WALL, 3, 0, 0);
	}
	render_table_add(H_CELLS - 1, V_CELLS - 1, WALL, 4, 0, 0);
	for (int i = H_CELLS - 1; i > 1; --i) {
		render_table_add(i - 1, V_CELLS - 1, WALL, 5, 0, 0);
	}
	render_table_add(0, V_CELLS - 1, WALL, 6, 0, 0);
	for (int i = V_CELLS - 1; i > 1; --i) {
		render_table_add(0, i - 1, WALL, 7, 0, 0);
	}
}

static void graphic_lut(enum directions direction,
                        enum directions previous_direction,
                        enum sprite_tags *sprite, unsigned char *rotation,
                        unsigned char *flip_y)
{
	*rotation = previous_direction;
	*flip_y = 0;

	if (direction != previous_direction) {  // Snake is bending at this point
		*sprite += NUM_BASE_SNAKE_SPRITES;  // Bending sprites come after base sprites

		if ((int) direction - (int) previous_direction == -1 ||
		    (int) direction - (int) previous_direction == 3)
		{
			*flip_y = 1;
		}
	}
}

static void game_running_render(void)
{
	Rectangle src = {.width = CELLSIZE};
	Rectangle dst = {.width = CELLSIZE, .height = CELLSIZE};
	static const Vector2 origin = {.x = CELLSIZE / 2.0f, .y = CELLSIZE / 2.0f};
	static const float rotation_table[] = {0.0f, -90.0f, 180.0f, 90.0f};
	float rotation;

	background_render();

	DrawTextEx(font, TextFormat("TIME%3d SCORE %7d speed %.4f", game_timer.value, game_score.game, snake.speed),
	           (Vector2){0.0f, V_CELLS * CELLSIZE}, FONTSIZE, 0.0f, WHITE);

	for (int i = 0; i < render.num_elements; ++i) {
		if (render.table[i].sprite == SCORE_GT) {
			DrawTextEx(font, TextFormat("+%d", fruit_score.render_score),
			           (Vector2){render.table[i].x * CELLSIZE,
			                     render.table[i].y * CELLSIZE},
			           /*(Vector2){render.table[i].x * CELLSIZE_I,
			                     render.table[i].y * CELLSIZE_I},*/
			           FONTSIZE, 0.0f, RED);

			continue;
		}
		else
		if (render.table[i].sprite == SCORE_GT_PANEL) {
			DrawRectangle(render.table[i].x * CELLSIZE_I,
			              render.table[i].y * CELLSIZE_I,
			              fruit_score.render_len * CELLSIZE_I, CELLSIZE_I, BLACK);

			continue;
		}

		src.y = render.table[i].sprite * CELLSIZE;
		src.x = render.table[i].step * CELLSIZE;
		dst.y = (render.table[i].y * CELLSIZE) + origin.y;
		dst.x = (render.table[i].x * CELLSIZE) + origin.x;
		/*src.y = render.table[i].sprite * CELLSIZE_I;
		src.x = render.table[i].step * CELLSIZE_I;
		dst.y = (render.table[i].y * CELLSIZE_I) + origin.y;
		dst.x = (render.table[i].x * CELLSIZE_I) + origin.x;*/
		rotation = rotation_table[render.table[i].rotation];

		if (render.table[i].flip_xy) {
			src.height = -CELLSIZE;  // Actually it's only flip y
		}
		else {
			src.height = CELLSIZE;
		}

		DrawTexturePro(snake_tex, src, dst, origin, rotation, WHITE);
	}
}

#define READY_MSG_LEN 6.0f

static void game_running_render_countdown(void)
{
	static const Vector2 msg_pos = {.x = ((H_CELLS / 2) - (INITIAL_LEN / 2) - 1) * CELLSIZE,
	                                .y = ((V_CELLS / 2) - 1) * CELLSIZE};
	static const Vector2 msg_panel_size = {.x = READY_MSG_LEN * CELLSIZE,
	                                       .y = 8.0f};
	static const char *countdown_msg[] = {"READY?", " !GO! "};
	const char **cur_msg = countdown_msg;

	game_running_render();

	if (global_count >= (INITIAL_COUNTDOWN / 2) + 1) {
		++cur_msg;
	}

	DrawRectangleV(msg_pos, msg_panel_size, BLACK);
	DrawTextEx(font, *cur_msg, msg_pos, FONTSIZE, 0.0f, RED);
}

#define PAUSE_MSG_LEN 18   // Length of the longest line
#define PAUSE_MSG_LINES 3  // Number of lines

static void game_running_render_pause(void)
{
	static const char *pause_msg[] = {"PAUSE", "ENTER:back to game", "BCKSP:back to menu"};
	static const Vector2 panel_size = {.x = PAUSE_MSG_LEN * CELLSIZE,
	                                   .y = PAUSE_MSG_LINES * CELLSIZE};

	Vector2 panel_pos = {
		.x = ((H_CELLS - PAUSE_MSG_LEN) / 2) * CELLSIZE_I,
		.y = ((V_CELLS - PAUSE_MSG_LINES) / 2) * CELLSIZE_I,
	};

	static int panel_blink = 1;

	game_running_render();

	panel_blink = 1 - panel_blink;

	if (panel_blink == 1) {
		DrawRectangleV(panel_pos, panel_size, BLACK);
	}

	for (size_t i = 0; i < sizeof(pause_msg) / sizeof(pause_msg[0]); ++i) {
		DrawTextEx(font, pause_msg[i], panel_pos, FONTSIZE, 0.0f, WHITE);
		panel_pos.y += FONTSIZE;
	}
}

#define GE_MSG_WI 20  // Number of characters the longest string of the end message has
#define GE_MSG_HE 8  // Number of lines the end message has
#define GE_MSG_X 6  // x position of the end message (in tiles)
#define GE_MSG_Y ((V_CELLS) / 2 - (GE_MSG_HE) / 2)  // y position of the end message (centered) (in tiles)
#define GE_HISCORE_Y ((GE_MSG_Y) - 3)  // y position of the high score message
#define GE_HISCORE_HE 3  // Number of lines of the high score message

static void game_running_render_end(void)
{
	static int panel_blink = 1;

	game_running_render();

	if (global_count < FINAL_COUNTDOWN) {
		return;
	}

	panel_blink = 1 - panel_blink;

	if (panel_blink == 1) {
		DrawRectangle(GE_MSG_X * CELLSIZE_I, GE_MSG_Y * CELLSIZE_I,
		              GE_MSG_WI * CELLSIZE_I, GE_MSG_HE * CELLSIZE_I, BLACK);
	}

	game_end_message(GE_MSG_X, GE_MSG_Y);

	const struct score mode_hscore = stt_get_mode_hi_score();

	if (game_score.final > mode_hscore.score) {
		if (panel_blink == 1) {
			DrawRectangle(GE_MSG_X * CELLSIZE_I, GE_HISCORE_Y * CELLSIZE_I,
			              GE_MSG_WI * CELLSIZE_I, GE_HISCORE_HE * CELLSIZE_I,
			              BLACK);

		}

		high_score_message(GE_MSG_X, GE_HISCORE_Y);
	}
}

static void game_end_message(const int x, const int y)
{
	const float xf = x * CELLSIZE;
	float yf = y * CELLSIZE - FONTSIZE;

	DrawTextEx(font, TextFormat("GAME SCORE  %8d", game_score.game),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("TIME BONUS  %8d", game_score.time_bonus),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("LENGTH BONUS%8d", game_score.length_bonus),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("RANK MULT   %8.1f", final_score_mod),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("TOTAL       %8d", game_score.final),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	yf += FONTSIZE;
	DrawTextEx(font, TextFormat("ENTER -> play again"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
	DrawTextEx(font, TextFormat("BCKSP -> go to menu"),
	           (Vector2){xf, yf += FONTSIZE}, FONTSIZE, 0.0f, WHITE);
}

static void high_score_message(const int x, const int y)
{
	DrawTextEx(font, TextFormat("NEW HIGH SCORE!"),
	           (Vector2){x * CELLSIZE, y * CELLSIZE}, FONTSIZE, 0.0f, RED);
	DrawTextEx(font, TextFormat(" ENTER NAME:"),
	           (Vector2){x * CELLSIZE, (y + 2) * CELLSIZE},
	            FONTSIZE, 0.0f, RED);
	DrawTextEx(font, TextFormat("%s", game_score.initials),
	           (Vector2){(x + 14) * CELLSIZE, (y + 1) * CELLSIZE},
	           FONTSIZE * 2.0f, 0.0f, WHITE);
}

static void game_running_exit(void)
{
	if (background_selection_is_random()) {
		background_set();
	}
}

static void show_score_macros(void)  // Debug
{
	TraceLog(LOG_INFO, TextFormat(
	"\nRANK_BASE_EASY=%d\n"
	"RANK_BASE_MEDIUM=%d\n"
	"RANK_BASE_HARD=%d",RANK_BASE_EASY, RANK_BASE_MEDIUM, RANK_BASE_HARD));

	TraceLog(LOG_INFO, TextFormat(
	"\nSPRINT_RATIO=%f\n"
	"SPRINT_BONUS_EASY=%f\n"
	"SPRINT_BONUS_MEDIUM=%f\n"
	"SPRINT_BONUS_HARD=%f", SPRINT_RATIO, SPRINT_BONUS_EASY,
	SPRINT_BONUS_MEDIUM, SPRINT_BONUS_HARD));

	TraceLog(LOG_INFO, TextFormat(
	"\nSLOW_PENALTY_EASY=%f\n"
	"SLOW_PENALTY_MEDIUM=%f\n"
	"SLOW_PENALTY_HARD=%f",SLOW_PENALTY_EASY, SLOW_PENALTY_MEDIUM,
	SLOW_PENALTY_HARD));

	TraceLog(LOG_INFO, TextFormat(
	"\nSPRINT_BONUS_SPEED=%f\n"
	"SLOW_PENALTY_SPEED=%f", SPRINT_BONUS_SPEED, SLOW_PENALTY_SPEED));

	TraceLog(LOG_INFO, TextFormat(
	"\nLENGTH_SCORE_RATIO=%d\n"
	"INITIAL_LEN=%d\n"
	"STARTING_FRUIT_STEP=%d\n"
	"DISTANCE_BONUS_RATIO=%d\n"
	"FRUIT_SCORE_SUB=%d",LENGTH_SCORE_RATIO,
	INITIAL_LEN, STARTING_FRUIT_STEP, DISTANCE_BONUS_RATIO, FRUIT_SCORE_SUB));

	TraceLog(LOG_INFO, TextFormat("\nWALL_MODIFIER=%f", WALL_MODIFIER));
}
