/* audio.h */

#ifndef AUDIO_H
#define AUDIO_H

#include <raylib.h>


enum sounds {
	SOUND_EAT_FRUIT,
	SOUND_SPRINT_READY,
	SOUND_SPRINT_ACTIVE,
	SOUND_SNAKE_COLLISION,
	SOUND_TIME_OUT,
	SOUND_READY,
	SOUND_GO,
	SOUND_MENU_ENTER,
	SOUND_MENU_BACK,
	SOUND_MENU_VERT,
	SOUND_MENU_HORI,
	NUM_SOUNDS,
};

enum songs {
	SONG_TITLE_SCREEN = 0,
	SONG_GAMEPLAY_1,
	SONG_GAMEPLAY_2,
	NUM_SONGS,
	SONG_NONE = NUM_SONGS,
};


extern Sound sounds[];

// Made extern so the soundtest_tool program can read its values
extern const float sfx_mix_volume;
extern const float song_mix_volumes[];


void audio_init(void);
void audio_play_song(enum songs song);
bool audio_is_music_playing(void);  // Any song
void audio_set_song_volume(float volume);
int audio_get_sfx_user_volume(void);
int audio_get_music_user_volume(void);
int audio_set_sfx_user_volume(int user_volume);
int audio_set_music_user_volume(int user_volume);
void audio_update_music(void);
void audio_close(void);


#endif