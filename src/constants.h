/* constants.h */

#ifndef CONSTANTS_H
#define CONSTANTS_H


#define V_CELLS 23
#define H_CELLS 32
/*#define V_CELLS 27
#define H_CELLS 40*/
#define S_V_CELLS (V_CELLS + 1)
#define S_H_CELLS H_CELLS
#define TO_FLOAT(n) n##.0f
#define TO_FLOAT_EXPAND(n) TO_FLOAT(n)
#define CELLSIZE_I 8
#define CELLSIZE TO_FLOAT_EXPAND(CELLSIZE_I)
#define FONTSIZE 8.0f
#define DFT_FPS 60

#define KEY_OK KEY_ENTER
#define KEY_GOBACK KEY_BACKSPACE


#endif